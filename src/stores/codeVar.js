import { reactive, ref } from "vue";

export default {
    state: reactive({
        // 存储每个动画的变量监视
        // [0]存储所需监视变量名
        // [1]存储具体变量名的值
        bubble: [{
            header: 'i',
            data: ''
        }, {
            header: 'j',
            data: ''
        }],

        choice: [{
            header: 'i',
            data: ''
        }, {
            header: 'j',
            data: ''
        }, {
            header: 'minIndex',
            data: ''
        }],


        guibing: [{
            header: 'l',
            data: ''
        }, {
            header: 'r',
            data: ''
        }, {
            header: 'mid',
            data: ''
        }],


        insert: [{
            header: 'i',
            data: ''
        }, {
            header: 'j',
            data: ''
        }, {
            header: 'temp',
            data: ''
        }],


        josephring: [{
            header: 'now',
            data: ''
        }, {
            header: 'count',
            data: ''
        }, {
            header: 'prev_now',
            data: ''
        }],


        hashlist1: [{
            header: 'i',
            data: ''
        }, {
            header: 'j',
            data: ''
        }, {
            header: '插入的位置',
            data: ''
        }],


        hashlist2: [{
            header: 'i',
            data: ''
        }, {
            header: '插入的位置',
            data: ''
        }],


        kuohao: [{
            header: '等待匹配长度',
            data: ''
        }, {
            header: '栈长度',
            data: ''
        }, {
            header: 'now',
            data: ''
        }],


        maze: [{
            header: 'tx',
            data: ''
        }, {
            header: 'ty',
            data: ''
        }, {
            header: 'next_x',
            data: ''
        }, {
            header: 'next_y',
            data: ''
        }],

        shunxulistinsert: [{
            header: '插入位置',
            data: ''
        }, {
            header: '表长度',
            data: ''
        }],


        shunxulistdelete: [{
            header: '删除位置',
            data: ''
        }, {
            header: 'i',
            data: ''
        }, {
            header: 'j',
            data: ''
        }],


        tree1: [{
            header: '栈长度',
            data: ''
        }, {
            header: '当前根节点',
            data: ''
        }],


        tree2: [{
            header: '栈长度',
            data: ''
        }, {
            header: '当前根节点',
            data: ''
        }],


        tree3: [{
            header: '栈长度',
            data: ''
        }, {
            header: '当前根节点',
            data: ''
        }],


        tree4: [{
            header: '当前根节点',
            data: ''
        }, ],


        prim: [{
            header: '确定节点数',
            data: ''
        }, {
            header: '当前边',
            data: ''
        }, ],

        krusal: [{
            header: '确定节点数',
            data: ''
        }, {
            header: '当前边',
            data: ''
        }, ]


    }),

}