export default {
    state: {
        // 存储每个动画的伪代码
        bubble: [{
                "id": 1,
                "class": "code",
                "code_txt": "for(let i=0;i<待排序数组的长度-1;i++){"
            },
            {
                "id": 2,
                "class": "code",
                "code_txt": "    for(letj=0;j<待排序数组的长度-1;j++){"
            },
            {
                "id": 3,
                "class": "code",
                "code_txt": "        if(左边元素>右边元素){"
            },
            {
                "id": 4,
                "class": "code",
                "code_txt": "            swap(左边对象,右边对象)"
            },
            {
                "id": 5,
                "class": "code",
                "code_txt": "        }"
            },
            {
                "id": 6,
                "class": "code",
                "code_txt": "}"
            },
        ],
        choice: [{
            "id": 1,
            "class": "code",
            "code_txt": "let minIndex = 0;"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "for(let i=0;i<待排序数组长度-1;i++){"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    minIndex = i;"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "    for(let j=i+1;j<待排序数组长度;j++){"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "        if(第j位元素小于当前已知最小元素){"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "            minIndex = j;"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "    交换第i位和第minIndex的数值"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "}"
        }],
        guibing: [{
            "id": 1,
            "class": "code",
            "code_txt": "bulies(l, r) {"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    if (r == l) return;"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    let mid = l + r >> 1; //递归"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "    bulies(l, mid);bulies(mid + 1, r);"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "    let times = []; //临时数组"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "    for (let i = l; i <= mid; i++) {"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "        if (左区间代码>右区间代码) {"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "            右区间元素进临时数组"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "            i--;y++;"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "        } else {//左区间代码<=右区间代码"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "            左区间元素进临时数组"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "    while (y <= r) {"
        }, {
            "id": 15,
            "class": "code",
            "code_txt": "       右区间剩余元素进临时数组"
        }, {
            "id": 16,
            "class": "code",
            "code_txt": "    } "
        }, {
            "id": 17,
            "class": "code",
            "code_txt": "    for (let i = l; i<=r;i++){"
        }, {
            "id": 18,
            "class": "code",
            "code_txt": "        赋值给原数组"
        }, {
            "id": 19,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 20,
            "class": "code",
            "code_txt": "}"
        }],
        insert: [{
            "id": 1,
            "class": "code",
            "code_txt": "for(i=1;i<待排序数组长度;i++){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    if (当前元素小于前一个元素){"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "        temp=当前元素"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "    for(j=i-1;j>=0&&当前元素a>temp;j--){"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "            交换两个元素"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "}"
        }],
        hashlist1: [{
            "id": 1,
            "class": "code",
            "code_txt": "for(let i=0;i<待插入数组的长度;i++){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    插入的位置 = 获取到当前数值的哈希值"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    if (插入的位置没被占领){ "
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "        将当前值插入该位置"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "    }else{ //插入的位置被人占领了"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "     for(let j=插入位置+1;j<待插入长度;j++){"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "            if (当前遍历的位置没被占领) {"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "                将当前值插入该位置"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "                break;"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "            }"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "}"
        }],
        hashlist2: [{
            "id": 1,
            "class": "code",
            "code_txt": "for(let i=0;i<待插入数组的长度;i++) {"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    插入的位置 = 获取到当前数值的哈希值"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    if(插入的位置没被占领){"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "        将当前值插入该位置"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "    }else{ //插入的位置被人占领了"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "        将当前值插入到要插入的位置的链表末端"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "}"
        }],
        kuohao: [{
            "id": 1,
            "class": "code",
            "code_txt": "while(当前等待匹配字符串长度>0){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    let now = 当前待匹配的数组首个元素"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    if(now是左括号) {"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "        将now入栈"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "    }else{"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "        if(栈为空) {"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "            栈为空，无法匹配"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "            return"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "        if(now刚好匹配栈顶元素) {"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "            右括号入栈,栈内有匹配左括号"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "            将栈顶元素出栈"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "        }else{"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "            匹配失败"
        }, {
            "id": 15,
            "class": "code",
            "code_txt": "            return"
        }, {
            "id": 16,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 17,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 18,
            "class": "code",
            "code_txt": "}"
        }],
        maze: [{
            "id": 1,
            "class": "code",
            "code_txt": "dfs(tx, ty) {"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    if(tx==终点x坐标&&ty==终点y坐标){"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "        return true;"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "    for(let i=0;i<4;i++){"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "        let next_x = 下一个方向的x坐标"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "        let next_y = 下一个方向的y坐标"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "        if(next不是障碍物&&没有被访问){"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "            将下一个点标记为访问过"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "            dfs(next_x, next_y);"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "            将下一个点标记为没访问过"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "}"
        }],
        shunxulistinsert: [{
            "id": 1,
            "class": "code",
            "code_txt": "for(let i=0;i< this.data.length;i++){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    if(当前可以直接插入){"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "        直接插入"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "   }else{"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "        //当前位置元素往后挪"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "        for(let j=0;j< 数组长度;j++){"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "            if(找到一个为空的位置){"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "                for(let z=j;z>插入的位置;z--){"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "                  每一位向后挪"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "               }"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "               直接插入"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "                break"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "            }"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 15,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 16,
            "class": "code",
            "code_txt": "}"
        }],
        shunxulistdelete: [{
            "id": 1,
            "class": "code",
            "code_txt": "for(let i=0;i<待删除数组长度;i++){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    if(删除的位置等于数组下标){"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "        move_to(删除的元素向下移动)"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "        for(let j=后一位;j<数组的长度;j++){"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "            if(第i+1位不为空){"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "            move_to(从该元素开始从后往前移动)"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "            }else{"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "                break;"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "            }"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "        destory(消除元素);"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "        break;"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "}"
        }],
        josephring: [{
            "id": 1,
            "class": "code",
            "code_txt": "let now = 0;"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "let count = 0;"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "let prev_now = 0;"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "while (当前小圆圈个数>0){"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "    count++;"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "    if (当前叫到删除的数){"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "        将当前小圆圈删除"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "    now++;"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "    prev_now++;"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "}"
        }],
        tree1: [{
            "id": 1,
            "class": "code",
            "code_txt": "while (stack.length | | root){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "if (该节点存在){"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    stack.push(root);//将该点进栈"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "    root=root * 2;//进入左儿子"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "    if (root-1>=this.circles.length)"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "    root=0;"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "}"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "else {"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "    root=stack.pop();//出栈"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "    root=root * 2+1;//进入右儿子"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "   if(root-1>=this.circles.length)"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "        root=0;"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "}"
        }],
        tree2: [{
            "id": 1,
            "class": "code",
            "code_txt": "while(stack.length||root){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    while(root){"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "        stack.push(root);"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "        root=root*2;"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "        if(root-1>=this.circles.length)"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "        root=0;"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "    }//进入最左儿子"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "    if(stack.length){"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "        root=stack.pop();"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "        //出栈记录答案"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "        root=root*2+1;//进入右儿子"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "        if(root-1>=this.circles.length)"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "            root=0;"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 15,
            "class": "code",
            "code_txt": "}"
        }],
        tree3: [{
            "id": 1,
            "class": "code",
            "code_txt": "while(root){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    stack.push(root);"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    root=root*2;"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "    if(root-1>=this.circles.length)"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "        root=0;"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "}//进入最左儿子"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "while(stack.length){"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "    root=stack.pop();"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "    let u=0;"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "    for(let i = 0; i<vis.length;i++)"
        }, {
            "id": 11,
            "class": "code",
            "code_txt": "        if(vis[i]==root*2+1)u=1;"
        }, {
            "id": 12,
            "class": "code",
            "code_txt": "    if(root*2>=this.circles.length||u)"
        }, {
            "id": 13,
            "class": "code",
            "code_txt": "        vis.push(root);"
        }, {
            "id": 14,
            "class": "code",
            "code_txt": "    else {"
        }, {
            "id": 15,
            "class": "code",
            "code_txt": "        stack.push(root);root=root*2+1;"
        }, {
            "id": 16,
            "class": "code",
            "code_txt": "        while(root){"
        }, {
            "id": 17,
            "class": "code",
            "code_txt": "            stack.push(root);root=root*2;"
        }, {
            "id": 18,
            "class": "code",
            "code_txt": "            if(root-1>=this.circles.length)"
        }, {
            "id": 19,
            "class": "code",
            "code_txt": "                root=0;"
        }, {
            "id": 20,
            "class": "code",
            "code_txt": "        }//进入最左儿子"
        }, {
            "id": 21,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 22,
            "class": "code",
            "code_txt": "}"
        }],
        tree4: [{
            "id": 1,
            "class": "code",
            "code_txt": "for(let i=0;i< this.datas.length;i++){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    将当前儿子push进栈"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "  }"
        }],
        prim: [{
            "id": 1,
            "class": "code",
            "code_txt": "prim(){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    stack.push(0);"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    while(点集合是否满){"
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "        寻找集合内的点连接集合外点的最小边"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "        将该点加入集合"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "    }"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "}"
        }, ],
        krusal: [{
            "id": 1,
            "class": "code",
            "code_txt": "kruskal(){"
        }, {
            "id": 2,
            "class": "code",
            "code_txt": "    将边权排序"
        }, {
            "id": 3,
            "class": "code",
            "code_txt": "    for(let i=0;i< lens-1;i++){ "
        }, {
            "id": 4,
            "class": "code",
            "code_txt": "        for(let j=0;j< this.lines.length;j++){"
        }, {
            "id": 5,
            "class": "code",
            "code_txt": "            选中边"
        }, {
            "id": 6,
            "class": "code",
            "code_txt": "            判断边的两端是否都在集合内"
        }, {
            "id": 7,
            "class": "code",
            "code_txt": "            若不在，则选中该边加入集合"
        }, {
            "id": 8,
            "class": "code",
            "code_txt": "        }"
        }, {
            "id": 9,
            "class": "code",
            "code_txt": "        将选中的边的两端加入集合"
        }, {
            "id": 10,
            "class": "code",
            "code_txt": "}"
        }]
    },
    getters: {

    },
    mutations: {

    },
    actions: {},
    modules: {}


}