export default {
    state: {
        now_code_running_name: "prim", //当前演示的动画名称
        now_code_running_data: [], // 当前准备演示的测试数组
        now_code_monitor: [], // 当前演示代码的伪代码
    },
    getters: {

    },
    mutations: {
        update_now_code_running_data(state, datas) {
            state.now_code_running_data = datas;
        },
        update_now_code_running_name(state, name) {
            state.now_code_running_name = name;
        }
    },
    actions: {},
    modules: {}


}