import { createStore } from 'vuex'
import ModuleMessage from './message'
import ModuleDatas from './datas'
import ModuleCodes from './codes'
import ModuleCodeVar from './codeVar'

export default createStore({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        message: ModuleMessage,
        datas: ModuleDatas,
        codes: ModuleCodes,
        code_var: ModuleCodeVar,
    }
})