import { GameObject } from './../../GameObject';
import { rectangle } from './../../rectangle';
import { ElMessage } from 'element-plus'


export class bubble extends GameObject {
    constructor(ctx, datas) {
        super();

        this.ctx = ctx; // 画布
        this.datas = datas; // 待排序数据

        // 初始数据
        this.rectWidth = 40; // 矩形的宽度
        this.rectHeight = 30; // 矩形的高度
        this.startX = 100; // 第一个数据的开始横坐标
        this.startY = 100; // 第一个数据的开始纵坐标
        this.distDelta = 10; // 每个矩形之间的间隔

        this.rectangles = []; //存放数据对象

        this.eps = 10; // 每一帧移动的距离
        this.speed = 100; // 每一帧移动速度
        this.status = "stop"; // 当前对象状态  stop:停止状态 running:运动状态 end:对象销毁
        this.has_called_start = true;

        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 100; // 代码监视的睡眠
        this.result = ''; // 动画演示后的结果 faild 查找失败 success 查找成功
        this.var_i = 0;
        this.var_j = 0;


        this.start();
    }

    computed_speed() {
        if (this.speed === 100) {
            return 100;
        } else if (this.speed === 200) {
            return 50;
        } else {
            return 200;
        }
    }

    loadDatas(datas) {
        let outer = this;
        let nowX = this.startX;
        let nowY = this.startY;

        let rectangleColor = "#FFE14D"; // 填充矩形颜色
        // 随机颜色
        // let colors = ["#FC512D", "#FFE14D", "#0B0E14", "#41B883", "#EC407A", "#16A085", "#5B2C6F", "#F1C40F"];

        for (let i = 0; i < datas.length; i++) {
            // const randomIndex = Math.floor(Math.random() * colors.length)
            // const randomColor = colors[randomIndex]



            this.rectangles.push(new rectangle(this.ctx, this.rectangles, i, nowX, nowY, this.rectWidth, datas[i] * 30, datas[i], rectangleColor));
            nowX = nowX + this.rectWidth + this.distDelta;
            // nowY = nowY + this.rectHeight + this.distDelta;
        }


    }



    async start() {
        this.loadDatas(this.datas);


    }

    async work() {
        this.now_runing = 0;

        await this.bubbleSort();
        this.status = 'end';
        this.result = 'success';
        this.now_runing = -1;
        ElMessage({
            showClose: true,
            message: "动画演示完成",
            type: 'success',
            duration: 2000,
        })
    }

    update() {
        this.update_status();
        this.update_spped();
        if (this.status !== "end") {
            this.render();
        }
    }

    update_status() {
        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            obj.status = this.status
        }
    }

    update_spped() {
        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            obj.speed = this.speed;
        }
    }



    async bubbleSort() {
        const rectangles = this.rectangles;





        this.var_i = 0;
        for (let i = 0; i < rectangles.length - 1; i++, this.var_i++) {
            this.now_runing = 1
            await this.sleep(this.computed_speed());
            this.var_j = 0;
            for (let j = 0; j < rectangles.length - 1; j++, this.var_j++) {
                this.now_runing = 2
                await this.sleep(this.computed_speed());

                this.now_runing = 3
                await this.sleep(this.computed_speed());

                if (rectangles[j].data > rectangles[j + 1].data) {
                    let tmpx1 = rectangles[j].x;
                    let tmpindex1 = rectangles[j].index;
                    let tmpx2 = rectangles[j + 1].x;
                    let tmpindex2 = rectangles[j + 1].index;
                    rectangles[j].color = rectangles[j + 1].color = "#48A049"


                    this.now_runing = 4;
                    await rectangles[j].move_to(tmpx2, rectangles[j + 1].y);
                    await rectangles[j + 1].move_to(tmpx1, rectangles[j].y);

                    rectangles[j].index = tmpindex2;
                    rectangles[j + 1].index = tmpindex1;
                    // rectangles[j].data = tmpdata2;
                    // rectangles[j + 1].data = tmpdata1;
                    rectangles[j].color = rectangles[j + 1].color = "#FFE14D"
                    this.render();
                }
            }
        }
        this.now_runing = -1

        // rectangles[2].move_to(400, 100);
    }


    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }
    on_destory() {
        while (this.rectangles.length > 0) {
            const obj = this.rectangles[0];
            obj.destory();
        }
    }

    render() {

        // this.ctx.fillStyle = "rgba(255, 255, 255,0.2)";
        this.ctx.fillStyle = "rgb(255, 255, 255)";

        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.rectangles.sort((a, b) => a.index - b.index);
        // 清空当前的
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            //画字
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }
    }
}