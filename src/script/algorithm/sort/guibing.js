import { GameObject } from './../../GameObject';
import { rectangle } from './../../rectangle';
import { ElMessage } from 'element-plus'



export class guibing extends GameObject {
    constructor(ctx, datas) {
        super();
        this.ctx = ctx;
        this.datas = datas;
        this.circleR = 10;
        this.startX = 100;
        this.startY = 100;
        this.disdelta = 10;
        this.rectangles = [];
        this.rectWidth = 40; // 矩形的宽度
        this.rectHeight = 30; // 矩形的高度
        this.nowY = 400; //将他移动到(x,nowy)
        this.max_rect_hight = 0; // 当前矩形的最长长度
        this.has_called_start = true; //有没有执行过

        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 100; // 代码监视的睡眠
        this.speed = 150;
        this.status = 'stop'; // stop:停止运动  running:正在运动  end:结束运动

        this.var_l = 0;
        this.var_r = 0;
        this.var_mid = 0;

        this.start();
    }

    computed_speed() {
        if (this.speed === 150) {
            return 150;
        } else if (this.speed === 300) {
            return 75;
        } else {
            return 300;
        }
    }

    loadDatas(datas) {
        let nowX = this.startX;
        let nowY = this.startY;
        let rectangleColor = "#FFE14D"; // 填充矩形颜色
        for (let i = 0; i < datas.length; i++) {
            this.rectangles.push(new rectangle(this.ctx, this, i, nowX, nowY, this.rectWidth, datas[i] * 20, datas[i], rectangleColor));
            nowX += this.rectWidth + this.disdelta;
            this.max_rect_hight = Math.max(this.max_rect_hight, datas[i] * 20);
        }
    }

    async start() {
        this.loadDatas(this.datas);

    }

    async work() {
        this.now_runing = 0;

        await this.bulies(0, this.rectangles.length - 1);
        this.status = 'end';
        this.now_runing = -1;
        ElMessage({
            showClose: true,
            message: "动画演示完成",
            type: 'success',
            duration: 2000,
        })
    }

    update() {
        this.update_status();
        if (this.status !== 'end') {
            this.render();

        }
    }
    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update_status() {
        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            obj.status = this.status;
            obj.speed = this.speed;
        }
    }


    async bulies(l, r) {
        const baseDate = this.rectangles;
        if (r == l) {
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            return;
        }

        this.now_runing = 3;
        await this.sleep(this.computed_speed());
        let mid = l + r >> 1; //递归

        this.now_runing = 4;
        await this.sleep(this.computed_speed());
        await this.bulies(l, mid);
        await this.bulies(mid + 1, r);
        this.var_l = l + 1;
        this.var_r = r + 1;
        this.var_mid = mid + 1;
        this.now_runing = 5;
        await this.sleep(this.computed_speed());
        let times = []; //临时数组
        let x = l;
        let y = mid + 1;
        let nowX = baseDate[l].x;
        let nowY = baseDate[0].y + this.max_rect_hight;
        let preY = baseDate[l].y;
        this.now_runing = 6;
        await this.sleep(this.computed_speed());
        for (let i = l; i <= mid; i++) {
            this.now_runing = 7;
            await this.sleep(this.computed_speed());
            if (y <= r && baseDate[y].data < baseDate[i].data) {
                this.now_runing = 8;
                await this.sleep(this.computed_speed());
                times.push(baseDate[y].index);
                baseDate[y].color = "#48A049";
                await baseDate[y].move_to(nowX, nowY);
                this.now_runing = 9;
                await this.sleep(this.computed_speed());
                i--;
                y++;
            } else {
                times.push(baseDate[i].index);
                baseDate[i].color = "#48A049";
                this.now_runing = 11;
                await this.sleep(this.computed_speed());
                await baseDate[i].move_to(nowX, nowY);
            }
            nowX += this.rectWidth + this.disdelta;
        }
        this.now_runing = 14;
        await this.sleep(this.computed_speed());
        while (y <= r) {
            this.now_runing = 15;
            await this.sleep(this.computed_speed());
            times.push(baseDate[y].index);
            baseDate[y].color = "#48A049";
            await baseDate[y].move_to(nowX, nowY);
            y++;
            nowX += this.rectWidth + this.disdelta;
        } //归并
        ///////////////////////////////////////
        let cnt = 0;
        for (let i = l; i <= r; i++) {
            baseDate[times[cnt++]].index = i;
        }
        this.render();
        this.now_runing = 17;
        await this.sleep(this.computed_speed());
        for (let i = l; i <= r; i++) {
            this.now_runing = 18;
            await this.sleep(this.computed_speed());
            await baseDate[i].move_to(baseDate[i].x, preY);
            baseDate[i].color = "#FFE14D";
        }
        this.render();
    }

    on_destory() {
        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            obj.destory();
        }
    }


    render() {

        this.ctx.fillStyle = "rgb(255, 255, 255)";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        this.rectangles.sort((a, b) => a.index - b.index);
        // 清空当前的

        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            //画字
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }
    }
}