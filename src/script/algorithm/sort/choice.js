import { GameObject } from '../../GameObject';
import { rectangle } from '../../rectangle';
import { ElMessage } from 'element-plus'


//已改成简单选择排序
export class choice extends GameObject {
    constructor(ctx, datas) {
        super();

        this.ctx = ctx; // 画布
        this.datas = datas; // 待排序数据

        // 初始数据
        this.rectWidth = 40; // 矩形的宽度
        this.rectHeight = 30; // 矩形的高度
        this.startX = 100; // 第一个数据的开始横坐标
        this.startY = 100; // 第一个数据的开始纵坐标
        this.distDelta = 10; // 每个矩形之间的间隔

        this.rectangles = []; //存放数据对象

        this.eps = 10; // 每一帧移动的距离
        this.speed = 100; // 每一帧移动速度
        this.has_called_start = true;

        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 200; // 代码监视的睡眠
        this.status = 'stop'; // stop:停止运动  running:正在运动  end:结束运动

        this.var_i = 0;
        this.var_j = 0;
        this.var_minIndex = 0;

        this.start();

    }
    computed_speed() {
        if (this.speed === 100) {
            return 100;
        } else if (this.speed === 200) {
            return 50;
        } else {
            return 200;
        }
    }


    loadDatas(datas) {
        let outer = this;
        let nowX = this.startX;
        let nowY = this.startY;

        let rectangleColor = "#ADD6E6"; // 填充矩形颜色
        // 随机颜色

        for (let i = 0; i < datas.length; i++) {


            this.rectangles.push(new rectangle(this.ctx, this.rectangles, i, nowX, nowY, this.rectWidth, datas[i] * 20, datas[i], rectangleColor));
            nowX = nowX + this.rectWidth + this.distDelta;
        }

    }



    async start() {
        this.loadDatas(this.datas);
    }
    async work() {
        this.now_runing = 0;

        await this.choiceSort();
        this.status = 'end';
        this.now_runing = -1;
        ElMessage({
            showClose: true,
            message: "动画演示完成",
            type: 'success',
            duration: 2000,
        })
    }

    update() {
        this.update_status();
        if (this.status !== 'end') {
            this.render();

        }
    }
    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update_status() {
        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            obj.status = this.status;
            obj.speed = this.speed;
        }
    }

    async choiceSort() {

        let minIndex = 0;
        const color_begin = "#ADD6E6";
        const color_running = "#0D8000";
        const color_min = "#DA2A38";
        const color_sure = "#FEA900";
        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        let i = 0,
            j = 0;
        for (i = 0, this.var_i = 0; i < this.rectangles.length - 1; i++, this.var_i++) {
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            this.now_runing = 3;
            await this.sleep(this.computed_speed());
            minIndex = i;
            this.var_minIndex = minIndex;
            this.rectangles[i].color = color_min;
            this.render();
            this.now_runing = 4;
            await this.sleep(this.computed_speed());
            for (j = i + 1, this.var_j = i + 1; j < this.rectangles.length; j++, this.var_j++) {
                this.rectangles[j].color = color_running;
                this.now_runing = 5;
                await this.sleep(this.computed_speed() * 2);
                if (this.rectangles[j].data < this.rectangles[minIndex].data) {
                    this.rectangles[minIndex].color = color_begin;
                    this.now_runing = 6;
                    await this.sleep(this.computed_speed());
                    minIndex = j;
                    this.var_minIndex = minIndex;
                    this.rectangles[minIndex].color = color_min;
                }
                if (j !== minIndex) {
                    this.rectangles[j].color = color_begin;
                }
            }
            this.now_runing = 9;
            await this.sleep(this.computed_speed());
            let tmpx1 = this.rectangles[i].x;
            let tmpx2 = this.rectangles[minIndex].x;
            let tmpindex1 = this.rectangles[i].index;
            let tmpindex2 = this.rectangles[minIndex].index;
            await this.rectangles[i].move_to(tmpx2, this.rectangles[i].y);
            await this.rectangles[minIndex].move_to(tmpx1, this.rectangles[minIndex].y);
            this.rectangles[i].index = tmpindex2;
            this.rectangles[minIndex].index = tmpindex1;
            this.render();
            this.rectangles[i].color = color_sure;
        }
        this.rectangles[this.rectangles.length - 1].color = color_sure;
        this.render();
    }

    on_destory() {
        while (this.rectangles.length > 0) {
            const obj = this.rectangles[0];
            obj.destory();
        }
    }


    render() {

        this.ctx.fillStyle = "rgba(255, 255, 255,0.2)";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        this.rectangles.sort((a, b) => a.index - b.index);

        for (let i = 0; i < this.rectangles.length; i++) {
            const obj = this.rectangles[i];
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }
    }
}