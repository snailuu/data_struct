import { GameObject } from './../../GameObject';
import { line } from './../../line';
import { rectangle } from './../../rectangle';
import { ElMessage } from 'element-plus'


export class hashList extends GameObject {
    constructor(ctx, datas, mode) {
        super();

        this.ctx = ctx;
        this.datas = datas; //待插入的数组
        this.mode = mode; // 两种模式 1. 寻址法 2.拉链法

        this.wait = [];
        this.arrs = [];
        this.bigRects = [];
        this.sure_num = 0;

        // 空槽位
        this.big_rects_start_x = 100;
        this.big_rects_start_y = 100;
        this.big_rects_width = 50;
        this.big_rects_height = 50;
        this.big_rects_num = datas.length;

        // 等待数组
        this.wait_start_x = 100;
        this.wait_start_y = 400;
        this.wait_width = 40;
        this.wait_height = 40;

        // 移动过程中的坐标
        this.move_center_y = this.big_rects_start_y + (this.wait_start_y - this.big_rects_start_y) / 2
        this.error_message = "当前哈希值位：2";
        // this.has_called_start = true;

        this.speed = 100; // 每一帧移动速度
        this.status = "stop"; // 当前对象状态  stop:停止状态 running:运动状态 end:对象销毁
        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 100; // 代码监视的睡眠

        this.var_i = 0;
        this.var_j = 0;
        this.var_插入的位置 = 0;
    }

    computed_speed() {
        if (this.speed === 100) {
            return 100;
        } else if (this.speed === 200) {
            return 50;
        } else {
            return 200;
        }
    }

    loadDatas(datas) {
        // new line(this.ctx, 100, 100, 200, 100, '12', '#43A8DA', 5)


        //插槽
        let nowX = this.big_rects_start_x;
        let nowY = this.big_rects_start_y;
        let delta = 10;
        let color = '#1387F4';
        for (let i = 0; i < this.big_rects_num; i++) {
            this.bigRects.push(new rectangle(this.ctx, this.bigRects, i, nowX, nowY, this.big_rects_width, this.big_rects_height, i, color));
            this.bigRects[i].textColor = '#000';
            nowX = nowX + this.big_rects_width + delta;
        }

        //待插入数组
        nowX = this.wait_start_x;
        nowY = this.wait_start_y;
        color = '#E04D33';
        for (let i = 0; i < datas.length; i++) {
            this.wait.push(new rectangle(this.ctx, this.wait, i, nowX, nowY, this.wait_width, this.wait_height, datas[i], color));
            nowX = nowX + this.wait_width + delta;
        }

        //当前已经插入插槽的数组
        for (let i = 0; i < this.big_rects_num; i++) {
            this.arrs[i] = null;
        }
    }

    loadDatas_mode2() {
        //当前已经插入插槽的数组
        for (let i = 0; i < this.big_rects_num; i++) {
            this.arrs[i] = [];
            // for (let j = 0; j < this.big_rects_num; j++) {
            //     this.arrs[i][j] = null;
            // }
        }
    }
    start() {
        this.loadDatas(this.datas);

    }

    async work() {
        this.now_runing = 0;

        if (this.mode === 'A') {
            await this.move1();

        } else {
            this.loadDatas_mode2()
            await this.move2();
        }
        // this.status = 'end';
        // this.result = 'success';
        this.now_runing = -1;

        ElMessage({
            showClose: true,
            message: "动画演示完成",
            type: 'success',
            duration: 2000,
        })
    }

    async move1() {
        let width_delta = (this.big_rects_width - this.wait_width) / 2;
        let height_delta = (this.big_rects_height - this.wait_height) / 2;
        // await this.wait[0].move_to(this.wait[0].x, this.move_center_y);
        // let onex = this.big_rects_start_x + width_delta;
        let oney = this.big_rects_start_y + height_delta;
        // let twox = onex + this.wait_width + 4 * width_delta;
        // let twoy = oney;
        // await this.wait[0].move_to(this.big_rects_start_x + width_delta, this.big_rects_start_y + height_delta);
        // await this.wait[0].move_to(this.cal_move_x(0), oney);
        // this.bigRects[0].textColor = '#E04D33'
        // await this.wait[1].move_to(this.cal_move_x(1), oney);
        // this.bigRects[1].textColor = '#E04D33'

        let i = 0,
            j = 0;
        for (i = 0, this.var_i = i; i < this.wait.length; i++, this.var_i++) {
            this.now_runing = 1;
            await this.sleep(this.computed_speed());
            const obj = this.wait[i];
            this.sure_num = obj.data % this.bigRects.length;
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            let pos = this.sure_num;
            this.var_插入的位置 = pos;
            obj.color = '#8DC379';
            // 先往上走一点点
            await obj.move_to(obj.x, this.move_center_y);
            //寻找可插入的位置
            this.now_runing = 3;
            await this.sleep(this.computed_speed());
            if (this.arrs[pos] === null) { // 要插入的位置没人占领, 直接插入
                let move_x = this.cal_move_x(pos);
                this.now_runing = 4;
                await this.sleep(this.computed_speed());
                await obj.move_to(move_x, obj.y);
                await obj.move_to(move_x, oney);
                this.arrs[pos] = obj;
                this.bigRects[pos].textColor = '#E04D33';
            } else { // 往后找到第一个为空的位置
                this.now_runing = 5;
                await this.sleep(this.computed_speed());
                let flag = 0;
                //找后半部分
                for (j = pos + 1, this.var_j = j; j < this.arrs.length; j++, this.var_j++) {
                    this.now_runing = 6;
                    await this.sleep(this.computed_speed());
                    if (this.arrs[j] === null) {
                        flag = 1;
                        this.now_runing = 7;
                        await this.sleep(this.computed_speed());
                        this.var_插入的位置 = j;
                        let move_x = this.cal_move_x(j);
                        await obj.move_to(move_x, obj.y);
                        await obj.move_to(move_x, oney);
                        this.arrs[j] = obj;
                        this.bigRects[j].textColor = '#E04D33';
                        this.now_runing = 8;
                        await this.sleep(this.computed_speed());
                        break;
                    }
                }
                //找前半部分
                if (flag === 0) {
                    for (j = 0, this.var_j = j; j < pos; j++, this.var_j++) {
                        this.now_runing = 6;
                        await this.sleep(this.computed_speed());
                        if (this.arrs[j] === null) {
                            flag = 1;
                            this.now_runing = 7;
                            await this.sleep(this.computed_speed());
                            this.var_插入的位置 = j;
                            let move_x = this.cal_move_x(j);
                            this.now_runing = 8;
                            await this.sleep(this.computed_speed());
                            await obj.move_to(move_x, obj.y);
                            await obj.move_to(move_x, oney);
                            this.arrs[j] = obj;
                            this.bigRects[j].textColor = '#E04D33';

                            break;
                        }
                    }
                }

            }
            obj.color = '#E04D33'
                // 判断要插入的位置，往坐或右移动
                // 确定插入的位置，向上移动
        }

    }

    async move2() {
        let height_delta = (this.big_rects_height - this.wait_height) / 2;

        let oney = this.big_rects_start_y + height_delta;

        // await this.wait[0].move_to(this.cal_move_x(1), this.cal_move_y(0));
        // await this.wait[1].move_to(this.cal_move_x(1), this.cal_move_y(1));

        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        let i = 0,
            j = 0;
        for (i = 0, this.var_i = 0; i < this.wait.length; i++, this.var_i++) {
            const obj = this.wait[i];
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            this.sure_num = obj.data % this.bigRects.length;
            let pos = this.sure_num;
            this.var_插入的位置 = pos;
            obj.color = '#8DC379';
            // 先往上走一点点
            await obj.move_to(obj.x, this.move_center_y);
            this.now_runing = 3;
            await this.sleep(this.computed_speed());
            //寻找可插入的位置
            if (this.arrs[pos].length === 0) { // 要插入的位置没人占领, 直接插入
                let move_x = this.cal_move_x(pos);
                this.now_runing = 4;
                await this.sleep(this.computed_speed());
                await obj.move_to(move_x, obj.y);
                await obj.move_to(move_x, oney);
                this.arrs[pos][0] = obj;
                this.bigRects[pos].textColor = '#E04D33';
            } else { // 在当前位置的数组末尾插入
                this.now_runing = 6;
                await this.sleep(this.computed_speed());
                let move_x = this.cal_move_x(pos);
                let move_y = this.cal_move_y(this.arrs[pos].length);
                await obj.move_to(move_x, obj.y);
                await obj.move_to(move_x, move_y);
                this.arrs[pos][this.arrs[pos].length] = obj;
            }
            // 判断要插入的位置，往坐或右移动
            // 确定插入的位置，向上移动
            obj.color = '#E04D33'

        }
    }


    // 计算要移动到arr数组第i位的x坐标
    cal_move_x(i) {
        let width_delta = (this.big_rects_width - this.wait_width) / 2;
        return (this.big_rects_start_x + width_delta) + (this.wait_width + 4 * width_delta) * i
    }

    // 计算要移动到arr数组y方向的第i位的y坐标
    cal_move_y(i) {
        let height_delta = (this.big_rects_height - this.wait_height) / 2;
        return this.big_rects_start_y + this.arrs[0].length + height_delta + this.wait_height * i
    }


    update() {
        this.render();
        this.update_status();
        this.update_spped();
    }
    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update_status() {
        for (let i = 0; i < this.bigRects.length; i++) {
            const obj = this.bigRects[i];
            if (obj === null) continue;
            obj.status = this.status
        }
        for (let i = 0; i < this.wait.length; i++) {
            const obj = this.wait[i];
            if (obj === null) continue;

            obj.status = this.status
        }
        for (let i = 0; i < this.arrs.length; i++) {
            const obj = this.arrs[i];
            if (obj === null) continue;

            obj.status = this.status
        }
    }

    update_spped() {
        for (let i = 0; i < this.bigRects.length; i++) {
            const obj = this.bigRects[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
        for (let i = 0; i < this.wait.length; i++) {
            const obj = this.wait[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
        for (let i = 0; i < this.arrs.length; i++) {
            const obj = this.arrs[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
    }

    on_destory() {
        while (this.bigRects.length > 0) {
            const obj = this.bigRects[0];
            obj.destory();
        }
        while (this.wait.length > 0) {
            const obj = this.wait[0];
            obj.destory();
        }
        while (this.arrs.length > 0) {
            const obj = this.arrs[0];
            obj.destory();
        }
    }


    render() {
        this.ctx.fillStyle = "rgba(255, 255, 255,0.3)";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.fillStyle = '#FAC40C';
        this.ctx.textBaseline = 'middle';
        this.error_message = `当前哈希值为：${this.sure_num}`
        this.ctx.fillText(this.error_message, 20, 30);
        for (let i = 0; i < this.bigRects.length; i++) {
            const obj = this.bigRects[i];
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            //画字
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y - obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = obj.textColor;
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }

        for (let i = 0; i < this.wait.length; i++) {
            const obj = this.wait[i];
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            //画字
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }

        for (let i = 0; i < this.arrs.length; i++) {
            const obj = this.arrs[i];
            if (obj === null) continue;
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            //画字
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }
    }
}