import { GameObject } from "../../GameObject";
import { rectangle } from "../../rectangle";
import { ElMessage } from 'element-plus'


export class shunxulist extends GameObject {
    constructor(ctx, data, mode) {
        super();
        this.ctx = ctx;
        this.data = data;
        this.mode = mode;

        this.startx = 100;
        this.starty = 100;

        this.smallRectWidth = 40;
        this.smallRectHeight = 40;
        this.disDelta = 10;
        this.bigRectWidth = this.smallRectWidth + this.disDelta * 2;
        this.bigRectHeight = this.smallRectHeight + this.disDelta * 2;

        this.eps = 10;
        this.speed = 150;
        this.has_exist = 0;
        this.slots = []
        this.stand = []
        this.none = []

        this.status = "stop";
        this.has_called_start = true;
        this.error_message = "";

        this.start();

        this.var_插入位置 = 0;
        this.var_表长度 = 0;
        this.var_删除位置 = 0;
        this.var_j = 0;
        this.var_i = 0;
    }
    computed_speed() {
        if (this.speed === 150) {
            return 150;
        } else if (this.speed === 300) {
            return 75;
        } else {
            return 300;
        }
    }

    upload(data) {
        //渲染蓝色插槽
        let nowX = this.startx
        let nowY = this.starty
        let rectangleColor = '#1E90FF';
        for (let i = 0; i < 3; i++) {
            this.slots.push(new rectangle(this.ctx, this.slots, i, nowX, nowY, this.bigRectWidth, this.bigRectHeight, i, rectangleColor))
            nowX = nowX + this.bigRectWidth + this.disDelta
        }
        //渲染待插数组
        nowX = this.startx + this.disDelta;
        nowY = this.starty + this.disDelta;
        nowY = nowY + 300
        for (let i = 0; i < data.length; i++) {
            this.stand.push(new rectangle(this.ctx, this.stand, i, nowX, nowY, this.smallRectWidth, this.smallRectHeight, data[i], "#00FFFF"))
            nowX = nowX + this.smallRectWidth + this.disDelta
        }
        //定义none数组中的元素全为null
        for (let i = 0; i < 20; i++) {
            this.none[i] = null
        }
    }
    async start() {
        this.upload(this.data);
        this.status = "running";
        this.render();
    }

    async work(pos) {
        this.status = "running"
        this.now_runing = 0;

        if (this.mode === 'A') {
            await this.insert(pos);
            if (this.stand.length === 0) {
                this.status = 'end';
                ElMessage({
                    showClose: true,
                    message: "动画演示完成",
                    type: 'success',
                    duration: 2000,
                })
            }
        } else if (this.mode === 'B') {
            if (pos >= this.computed_none_length()) {
                ElMessage({
                    showClose: true,
                    message: `当前顺序表下标为${pos}并无元素, 删除失败`,
                    type: 'error',
                    duration: 2000,
                })
                return;
            }
            await this.delect(pos);
            if (this.none.length === 0) {
                this.status = 'end';
                ElMessage({
                    showClose: true,
                    message: "动画演示完成",
                    type: 'success',
                    duration: 2000,
                })
            }
        }

        this.now_runing = -1;

        ElMessage({
            showClose: true,
            message: "操作已经完成",
            type: 'success',
            duration: 2000,
        })
    }

    async insert_all_data() {
        while (this.slots.length < this.stand.length) {
            this.newBigRect();
            this.slots[this.slots.length - 1].color = "#1e90ff"
        }
        for (let i = 0; i < this.stand.length; i++) {
            this.none[i] = this.stand[i];
            this.none[i].parent = this.none;
            this.none[i].x = this.startx + this.disDelta + (this.smallRectWidth + this.disDelta * 3) * i;
            this.none[i].y = this.starty + this.disDelta
        }
        while (this.stand.length > 0) {
            this.stand.shift();
        }
        this.render();
    }

    update() {
        this.update_status();
        if (this.status !== 'end') {
            this.render();

        }
    }
    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update_status() {
        for (let i = 0; i < this.slots.length; i++) {
            const obj = this.slots[i];
            if (obj === null) continue;
            obj.status = this.status;
            obj.speed = this.speed;
        }
        for (let i = 0; i < this.stand.length; i++) {
            const obj = this.stand[i];
            if (obj === null) continue;
            obj.status = this.status;
            obj.speed = this.speed;
        }
        for (let i = 0; i < this.none.length; i++) {
            const obj = this.none[i];
            if (obj === null) continue;
            obj.status = this.status;
            obj.speed = this.speed;
        }
    }


    // 在slots数组后面增加一个对象
    newBigRect() {
        let nowX = this.startx + (this.bigRectWidth + this.disDelta) * this.slots.length
        let nowY = this.starty
        this.slots.push(new rectangle(this.ctx, this.slots, this.slots.length, nowX, nowY, this.bigRectWidth, this.bigRectHeight, this.slots.length, '#00FA9A'))

    }

    // 计算当前已经插入的数量
    computed_none_length() {
        let res = 0;
        for (let i = 0; i < this.none.length; i++) {
            if (this.none[i] === null) continue;
            res++;
        }
        return res;
    }


    async insert(pos) {
        pos = Number(pos);
        this.error_message = `当前插入的位置是：${pos}`;
        this.now_runing = 2;
        await this.sleep(this.computed_speed());
        this.var_插入位置 = pos;
        this.var_表长度 = this.computed_none_length();

        if (this.none[pos] == null) {
            while (this.slots.length <= pos) {
                this.newBigRect();
            }
            this.now_runing = 3;
            await this.sleep(this.computed_speed());
            await this.stand[0].move_to(this.startx + this.disDelta + (this.smallRectWidth + this.disDelta * 3) * pos, this.starty + this.disDelta)
            this.none[pos] = this.stand[0];
            this.none[pos].parent = this.none;

        } else {
            this.now_runing = 6;
            await this.sleep(this.computed_speed());
            for (let j = pos + 1; j < this.none.length; j++) {
                this.now_runing = 7;
                await this.sleep(this.computed_speed());
                if (this.none[j] == null) {
                    this.now_runing = 8;
                    await this.sleep(this.computed_speed());
                    for (let z = j; z > pos && z >= 0; z--) {
                        while (this.slots.length <= z) {
                            this.newBigRect();
                        }
                        this.now_runing = 9;
                        await this.sleep(this.computed_speed());
                        await this.none[z - 1].move_to(this.startx + this.disDelta + (this.smallRectWidth + this.disDelta * 3) * z, this.starty + this.disDelta)
                        this.none[z] = this.none[z - 1]
                    }
                    this.now_runing = 11;
                    await this.sleep(this.computed_speed());
                    await this.stand[0].move_to(this.startx + this.disDelta + (this.smallRectWidth + this.disDelta * 3) * (pos), this.starty + this.disDelta)
                    this.none[pos] = this.stand[0];
                    this.none[pos].parent = this.none;
                    this.now_runing = 12;
                    await this.sleep(this.computed_speed());
                    break
                }
            }
        }
        this.stand.shift();
        this.var_表长度 = this.computed_none_length();
    }


    async delect(res) {
        res = Number(res);
        this.error_message = `当前删除的位置是：${res}`;
        this.var_删除位置 = res;
        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        let i = 0;
        for (i = 0, this.var_i = 0; i < this.data.length; i++, this.var_i++) {
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            if (i === res) {
                this.now_runing = 3;
                await this.sleep(this.computed_speed());
                await this.none[i].move_to(this.startx + this.disDelta + (this.smallRectWidth + this.disDelta * 3) * i, this.starty + this.disDelta + 200)
                this.now_runing = 4;
                await this.sleep(this.computed_speed());
                let j = 0;
                for (j = i + 1, this.var_j = j; j < this.data.length; j++, this.var_j++) {
                    this.now_runing = 5;
                    await this.sleep(this.computed_speed());
                    if (this.none[j] != null) {
                        this.now_runing = 6;
                        await this.sleep(this.computed_speed());
                        await this.none[j].move_to(this.startx + this.disDelta + (this.smallRectWidth + this.disDelta * 3) * (j - 1), this.starty + this.disDelta)
                    } else {
                        // alert('下一个要删除的位置已经没元素了')
                        break;
                    }
                }
                this.now_runing = 11;
                await this.sleep(this.computed_speed());
                this.none[i].destory();
                this.render();
                this.now_runing = 12;
                await this.sleep(this.computed_speed());
                break;
            }
        }
    }

    on_destory() {
        while (this.slots.length > 0) {
            const obj = this.slots[0];
            obj.destory();
        }
        while (this.stand.length > 0) {
            const obj = this.stand[0];
            obj.destory();
        }
        for (let i = 0; i < this.none.length; i++) {
            const obj = this.none[i];
            if (obj === null) {
                continue;
            }
            obj.destory();
        }
    }


    render() {
        this.ctx.fillStyle = "rgb(255, 255, 255)";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        //蓝色插槽
        this.ctx.fillStyle = '#FF8C00';
        this.ctx.textBaseline = 'middle';
        this.ctx.fillText(this.error_message, 50, 50);
        for (let i = 0; i < this.slots.length; i++) {
            const obj = this.slots[i];
            // obj.status = 'end'
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#000000"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#000';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY - this.bigRectHeight * 4 / 5);
        }
        //待排序数组
        for (let i = 0; i < this.stand.length; i++) {
            const obj = this.stand[i];
            // obj.status = 'end'
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#000000"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }

        //空数组
        for (let i = 0; i < this.none.length; i++) {
            const obj = this.none[i];
            if (obj === null) continue;
            // obj.status = 'end'
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#000000"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }
    }
}