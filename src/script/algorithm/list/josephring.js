import { GameObject } from './../../GameObject';
import { circle } from '../../circle';
import { ElMessage } from 'element-plus'



export class josephring extends GameObject {
    constructor(ctx, datas, number) {
        super();

        this.ctx = ctx;
        this.datas = datas; // 待删除的数组
        this.number = number; // 每次要删除的数字


        this.circles = []; // 当前还存在的圆对象
        this.bigCircles = []; // 大圆圈坑位
        this.x = 300; // 大圆圆心x坐标
        this.y = 300; // 大圆圆心y坐标
        this.r = 120; // 大圆圆心半径长度

        this.error_message = `当前报数到 ${this.number} 会被删除`;
        this.error_message_color = "#FE8E06";

        this.speed = 100; // 每一帧移动速度
        this.status = "stop"; // 当前对象状态  stop:停止状态 running:运动状态 end:对象销毁
        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 50; // 代码监视的睡眠

        this.var_now = 0;
        this.var_count = 0;
        this.var_prev_now = 0;
    }

    loadDatas(datas) {
        let theta = 2 * Math.PI / this.datas.length;
        let nowX = this.x;
        let nowY = this.y - this.r;
        let nowR = 20;

        // let colors = ["#FC512D", "#FFE14D", "#0B0E14", "#41B883", "#EC407A", "#16A085", "#5B2C6F", "#F1C40F"];
        let color = "#39BAE6";
        let bigcolor = "#17202A";
        const angleStep = (2 * Math.PI) / datas.length;
        for (let i = 0; i < datas.length; i++) {
            // const randomIndex = Math.floor(Math.random() * colors.length)
            // const randomColor = colors[randomIndex];
            const theta = i * angleStep;
            nowX = this.x + this.r * Math.cos(theta);
            nowY = this.y + this.r * Math.sin(theta);
            this.circles.push(new circle(this.ctx, this.circles, nowX, nowY, nowR, i, datas[i], color));
            nowX = this.x + (this.r + nowR * 2) * Math.cos(theta);
            nowY = this.y + (this.r + nowR * 2) * Math.sin(theta);
            this.bigCircles.push(new circle(this.ctx, this.bigCircles, nowX, nowY, nowR, i, i, bigcolor))
        }

        // new circle(this.ctx, nowX, nowY, nowR, 0, 1, '#33BAE6')
        //     // const x = x1 + r * Math.cos(theta);
        //     // const y = y1 + r * Math.sin(theta);
        // nowX = Math.ceil(nowX + nowR * Math.cos(theta));
        // nowY = Math.ceil(nowY + nowR * Math.sin(theta));
        // new circle(this.ctx, nowX, nowY, nowR, theta, 2, '#A5729C')

    }

    async start() {
        this.loadDatas(this.datas);
    }


    async work() {
        this.now_runing = 0;

        await this.run();
        ElMessage({
            showClose: true,
            message: "动画演示完成",
            type: 'success',
            duration: 2000,
        })
        this.now_runing = -1;
        this.status = 'end';
    }

    computed_speed() {
        if (this.speed === 100) {
            return 100;
        } else if (this.speed === 200) {
            return 50;
        } else {
            return 200;
        }
    }

    async run() {


        let now = 0;
        this.var_now = now;
        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        let count = 0;
        this.var_count = count;
        this.now_runing = 2;
        await this.sleep(this.computed_speed());
        let prev_now = 0; // 当前删除的位置映射到原数组的下标
        this.var_prev_now = prev_now;
        this.now_runing = 3;
        await this.sleep(this.computed_speed());
        const move_color = "#7DCEA0";
        const del_color = "#E74C3C";

        while (this.circles.length > 0) {
            this.now_runing = 4;
            await this.sleep(this.computed_speed());
            count++;
            this.var_count++;
            this.now_runing = 5;
            await this.sleep(this.computed_speed());
            this.now_runing = 6;
            await this.sleep(this.computed_speed());
            if (count % this.number === 0) {
                this.circles[now].color = del_color;
                // for (let i = 0; i < this.bigCircles.length; i++) {
                //     if (this.bigCircles[i] === this.circles[now])
                // }
                this.now_runing = 7;
                await this.sleep(this.computed_speed());
                await this.sleep(3 * this.computed_speed());
                this.circles.splice(now, 1);
                now--;
                this.var_now--;
            }
            this.now_runing = 8;
            await this.sleep(this.computed_speed());
            now++;
            this.var_now++;
            this.now_runing = 9;
            await this.sleep(this.computed_speed());
            prev_now++;
            this.var_prev_now++;
            if (now >= this.circles.length) {
                now = 0;
                this.var_now = 0;
            }
            if (prev_now >= this.bigCircles.length) {
                prev_now = 0;
                this.var_prev_now = 0;
            }

            if (this.circles.length !== 0) {
                let tmpcolor = this.circles[now].color;
                this.circles[now].color = move_color;
                await this.sleep(3 * this.computed_speed());
                this.circles[now].color = tmpcolor;
            }


            await this.sleep(3 * this.computed_speed());
        }
    }

    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update() {
        this.render();
        this.update_spped();
        this.update_status();
    }

    update_status() {
        for (let i = 0; i < this.bigCircles.length; i++) {
            const obj = this.bigCircles[i];
            if (obj === null) continue;
            obj.status = this.status
        }
        for (let i = 0; i < this.circles.length; i++) {
            const obj = this.circles[i];
            if (obj === null) continue;

            obj.status = this.status
        }
    }

    update_spped() {
        for (let i = 0; i < this.bigCircles.length; i++) {
            const obj = this.bigCircles[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
        for (let i = 0; i < this.circles.length; i++) {
            const obj = this.circles[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
    }

    on_destory() {
        while (this.bigCircles.length > 0) {
            const obj = this.bigCircles[0];
            obj.destory();
        }
        while (this.circles.length > 0) {
            const obj = this.circles[0];
            obj.destory();
        }
    }


    render() {

        this.ctx.fillStyle = "rgb(255, 255, 255)";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.circles.sort((a, b) => a.index - b.index);

        this.ctx.fillStyle = this.error_message_color;
        this.ctx.textBaseline = 'middle';
        this.ctx.fillText(this.error_message, 50, 50);

        for (let i = 0; i < this.bigCircles.length; i++) {
            const obj = this.bigCircles[i];

            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";

            const textX = obj.x - 7 * obj.data.toString().length; // 文字起点横坐标
            const textY = obj.y; // 文字起点纵坐标
            this.ctx.fillStyle = obj.color;
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);

        }
        for (let i = 0; i < this.circles.length; i++) {
            const obj = this.circles[i];

            this.ctx.fillStyle = obj.color;

            this.ctx.beginPath();
            this.ctx.arc(obj.x, obj.y, obj.r, 0, 2 * Math.PI);
            // this.ctx.colsePath();

            this.ctx.fill();
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 24px Arial";

            const textX = obj.x - 7 * obj.data.toString().length; // 文字起点横坐标
            const textY = obj.y; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);

        }
    }
}