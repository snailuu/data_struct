import { circle } from '../../circle';
import { line } from '../../line';
import { GameObject } from '../../GameObject';
import { ElMessage } from 'element-plus'



export class tree extends GameObject {
    constructor(ctx, datas, mode) {
        super();
        this.ctx = ctx;
        this.datas = datas;
        this.circleR = 20;
        this.startX = 300;
        this.startY = 100;
        this.circles = [];
        this.lines = [];
        this.ansCircle = []; // 已经遍历过的圆圈
        this.ansStartX = 250;
        this.ansStartY = 450;
        this.floorX = 400; //层隔
        this.floorY = 100;

        this.error_message = "";
        this.mode = mode; // A:先序遍历 B:中序遍历 C:后序遍历

        this.eps = 10; // 每一帧移动的距离

        this.has_called_start = true; //有没有执行过

        this.speed = 100; // 每一帧移动速度
        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 25; // 代码监视的睡眠
        this.start();

        this.var_栈长度 = 0;
        this.var_当前根节点 = 0;

    }

    computed_speed() {
        if (this.speed === 100) {
            return 100;
        } else if (this.speed === 50) {
            return 200;
        } else {
            return 50;
        }
    }

    loadDatas(datas) { //数据为0表示当前没有这个儿子
        let lens = 1;
        let Xlens = 0;
        while (datas.length > Xlens) Xlens += lens, lens *= 2;
        while (datas.length <= Xlens) datas += '0';
        let nowX = this.startX;
        let nowY = this.startY;
        let currentX = this.floorX;
        let circleColor = "#FFE14D"; // 填充矩形颜色
        let tot = 0; //开了多少层
        let x = 1;
        let cnt = 0; //当前层有多少个节点
        for (let i = 0; i < datas.length; i++) {
            this.circles.push(new circle(this.ctx, this.circles, nowX + currentX * cnt, nowY, this.circleR, i, datas[i], circleColor));
            cnt++;
            if (cnt == x) {
                x *= 2;
                cnt = 0;
                currentX /= 2;
                nowX -= currentX / 2;
                nowY += this.floorY;
            }
        }
        for (let i = 0; i < datas.length; i++) {
            let index = i + 1;
            if (index * 2 - 1 < datas.length && this.circles[index * 2 - 1].data !== '0' && this.circles[i].data !== '0') {
                this.lines.push(new line(this.ctx, this.circles[i].x, this.circles[i].y, this.circles[index * 2 - 1].x, this.circles[index * 2 - 1].y,
                    `${i}-${index*2-1}`, "#409eff", 1, this.lines));
            }
            if (index * 2 < datas.length && this.circles[index * 2].data !== '0' && this.circles[i].data !== '0') {
                this.lines.push(new line(this.ctx, this.circles[i].x, this.circles[i].y, this.circles[index * 2].x, this.circles[index * 2].y,
                    `${i}-${index*2}`, "#409eff", 1, this.lines));
            }

        }


    }

    async start() {
        this.loadDatas(this.datas);

    }

    async work() {
        this.now_runing = 0;

        if (this.mode === 'A') {
            await this.bfsFront();
        } else if (this.mode === 'B') {
            await this.bfsMid();
        } else if (this.mode === 'C') {
            await this.bfsBack();
        } else {
            await this.bfsFloor();
        }
        1 == 1;
        this.status = 'end';
        this.now_runing = -1;

        ElMessage({
            showClose: true,
            message: "动画演示完成",
            type: 'success',
            duration: 2000,
        })
    }


    update() {
        this.update_status();
        if (this.status !== 'end') {
            this.render();

        }
    }
    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update_status() {
        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            if (obj === null) continue;

            obj.status = this.status;
            obj.speed = this.speed;
        }
        for (let i = 0; i < this.circles.length; i++) {
            const obj = this.circles[i];
            if (obj === null) continue;

            obj.status = this.status;
            obj.speed = this.speed;
        }
        for (let i = 0; i < this.ansCircle.length; i++) {
            const obj = this.ansCircle[i];
            if (obj === null) continue;

            obj.status = this.status;
            obj.speed = this.speed;
        }
    }
    async bfsFront() {
        this.error_message = "先序遍历的结果：";
        let stack = [];
        let root = 1;
        let Frontans = [];
        this.now_runing = 1;
        this.update_status();
        await this.sleep(this.computed_speed());
        this.var_栈长度 = stack.length;
        this.var_当前根节点 = this.circles[root - 1].data;
        while (stack.length || root) {
            // await this.sleep(300);
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            this.var_当前根节点 = root;

            if (root) {
                this.circles[root - 1].color = "#E74C3C";
                this.now_runing = 3;
                await this.sleep(this.computed_speed());
                Frontans.push(root - 1);
                this.var_当前根节点 = this.circles[root - 1].data;
                this.var_栈长度 = stack.length;

                const obj = this.circles[root - 1];
                if (obj.data != 0) {
                    this.ansCircle.push(new circle(this.ctx, this.ansCircle, obj.x, obj.y, obj.r, this.ansCircle.length, obj.data, obj.color));
                    await this.ansCircle[this.ansCircle.length - 1].move_to(this.ansStartX, this.ansStartY);
                    // this.render();
                    this.ansStartX += obj.r * 2 + 10;
                }

                stack.push(root);
                this.var_栈长度 = stack.length;

                this.now_runing = 4;
                await this.sleep(this.computed_speed());
                root = root * 2;
                //this.var_当前根节点 = this.circles[root-1].data;

                this.now_runing = 5;
                await this.sleep(this.computed_speed());
                if (root - 1 >= this.circles.length) {
                    this.now_runing = 6;
                    await this.sleep(this.computed_speed());
                    root = 0;
                    //this.var_当前根节点 = root;

                }
            } else {
                this.now_runing = 7;
                await this.sleep(this.computed_speed());
                root = stack.pop();
                this.var_栈长度 = stack.length;

                //this.var_当前根节点 = root;

                this.now_runing = 8;
                await this.sleep(this.computed_speed());
                root = root * 2 + 1;
                this.now_runing = 9;
                //this.var_当前根节点 = root;

                await this.sleep(this.computed_speed());
                if (root - 1 >= this.circles.length) {
                    this.now_runing = 10;
                    await this.sleep(this.computed_speed());
                    root = 0;
                    //this.var_当前根节点 = root;

                }

            }
        }

    }


    async bfsMid() {

        this.error_message = "中序遍历的结果：";
        let stack = [];
        let root = 1;
        let Frontans = [];
        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        this.var_栈长度 = stack.length;
        this.var_当前根节点 = this.circles[root - 1].data;
        while (stack.length || root) {
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            //this.var_当前根节点 = this.circles[root-1].data;
            while (root) {
                this.now_runing = 3;
                await this.sleep(this.computed_speed());
                stack.push(root);
                this.var_栈长度 = stack.length;

                //this.var_当前根节点 = root;

                this.now_runing = 4;
                root = root * 2;
                //this.var_当前根节点 = root;

                this.now_runing = 5;
                await this.sleep(this.computed_speed());
                if (root - 1 >= this.circles.length) {
                    this.now_runing = 6;
                    await this.sleep(this.computed_speed());
                    root = 0;
                    //this.var_当前根节点 = root;
                }

            }
            this.now_runing = 8;
            await this.sleep(this.computed_speed());
            if (stack.length) {
                this.now_runing = 9;
                await this.sleep(this.computed_speed());
                root = stack.pop();
                this.var_栈长度 = stack.length;

                this.var_当前根节点 = this.circles[root - 1].data;

                this.circles[root - 1].color = "#E74C3C";
                Frontans.push(root - 1);
                const obj = this.circles[root - 1];
                if (obj.data != 0) {
                    this.ansCircle.push(new circle(this.ctx, this.ansCircle, obj.x, obj.y, obj.r, this.ansCircle.length, obj.data, obj.color));
                    await this.ansCircle[this.ansCircle.length - 1].move_to(this.ansStartX, this.ansStartY);
                    // this.render();
                    this.ansStartX += obj.r * 2 + 10;
                }
                this.now_runing = 10;
                await this.sleep(this.computed_speed());
                root = root * 2 + 1;
                //this.var_当前根节点 = root;
                this.now_runing = 11;
                await this.sleep(this.computed_speed());
                if (root - 1 >= this.circles.length) {
                    this.now_runing = 12;
                    await this.sleep(this.computed_speed());
                    root = 0;
                    //this.var_当前根节点 = root;
                }

            }
        }

    }


    async bfsBack() {


        this.error_message = "后序遍历的结果：";
        let stack = [];
        let root = 1;
        let Frontans = [];
        let vis = [];
        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        this.var_栈长度 = stack.length;
        this.var_当前根节点 = this.circles[root - 1].data;
        while (root) {
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            stack.push(root);
            this.var_栈长度 = stack.length;

            this.now_runing = 3;
            await this.sleep(this.computed_speed());
            root = root * 2;
            //this.var_当前根节点 = root;

            this.now_runing = 4;
            await this.sleep(this.computed_speed());
            if (root - 1 >= this.circles.length) {
                this.now_runing = 5;
                await this.sleep(this.computed_speed());
                root = 0;
                //this.var_当前根节点 = root;

            }

        }

        this.now_runing = 7;
        await this.sleep(this.computed_speed());
        while (stack.length) {
            this.now_runing = 8;
            await this.sleep(this.computed_speed());
            root = stack.pop();
            //this.var_当前根节点 = root;

            this.var_栈长度 = stack.length;

            this.now_runing = 9;
            await this.sleep(this.computed_speed());
            let u = 0;
            this.now_runing = 10;
            await this.sleep(this.computed_speed());
            for (let i = 0; i < vis.length; i++) {
                this.now_runing = 11;
                await this.sleep(this.computed_speed());
                if (vis[i] == root * 2 + 1) u = 1;
            }
            this.now_runing = 12;
            await this.sleep(this.computed_speed());
            if (root * 2 >= this.circles.length || u) {
                this.now_runing = 13;
                await this.sleep(this.computed_speed());
                vis.push(root);

                this.var_当前根节点 = this.circles[root - 1].data;
                this.circles[root - 1].color = "#E74C3C";
                Frontans.push(root - 1);
                const obj = this.circles[root - 1];
                if (obj.data != 0) {
                    this.ansCircle.push(new circle(this.ctx, this.ansCircle, obj.x, obj.y, obj.r, this.ansCircle.length, obj.data, obj.color));
                    await this.ansCircle[this.ansCircle.length - 1].move_to(this.ansStartX, this.ansStartY);
                    // this.render();
                    this.ansStartX += obj.r * 2 + 10;
                }


            } else {
                this.now_runing = 14;
                await this.sleep(this.computed_speed());
                stack.push(root);
                this.var_栈长度 = stack.length;

                root = root * 2 + 1;
                //this.var_当前根节点 = root;

                this.now_runing = 15;
                await this.sleep(this.computed_speed());
                while (root) {
                    this.now_runing = 16;
                    await this.sleep(this.computed_speed());
                    stack.push(root);
                    this.var_栈长度 = stack.length;

                    root = root * 2;
                    //this.var_当前根节点 = root;

                    this.now_runing = 17;
                    await this.sleep(this.computed_speed());
                    if (root - 1 >= this.circles.length) {
                        this.now_runing = 18;
                        await this.sleep(this.computed_speed());
                        root = 0;
                        //this.var_当前根节点 = root;

                    }

                }
            }
        }
    }


    async bfsFloor() {
        this.error_message = "层次遍历的结果：";
        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        for (let i = 0; i < this.datas.length; i++) {
            const obj = this.circles[i];
            obj.color = "#E74C3C";
            if (obj.data != 0) {
                this.var_当前根节点 = this.circles[i].data;

                this.now_runing = 2;
                await this.sleep(this.computed_speed());
                this.ansCircle.push(new circle(this.ctx, this.ansCircle, obj.x, obj.y, obj.r, this.ansCircle.length, obj.data, obj.color));
                await this.ansCircle[this.ansCircle.length - 1].move_to(this.ansStartX, this.ansStartY);
                // this.render();
                this.ansStartX += obj.r * 2 + 10;
            }
        }



    }

    on_destory() {
        while (this.lines.length > 0) {
            const obj = this.lines[0];
            obj.destory();
        }
        while (this.circles.length > 0) {
            const obj = this.circles[0];
            obj.destory();
        }
        while (this.ansCircle.length > 0) {
            const obj = this.ansCircle[0];
            obj.destory();
        }
    }


    async render() {

        this.ctx.fillStyle = "rgb(255, 255, 255)";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        //this.rectangles.sort((a, b) => a.index - b.index);
        // 清空当前的

        this.ctx.fillStyle = "#E74C3C";

        this.ctx.textBaseline = 'middle';
        this.ctx.fillText(this.error_message, 0, 450);

        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            this.ctx.lineWidth = obj.line_width; //设置线宽状态
            this.ctx.beginPath();
            this.ctx.moveTo(obj.startX, obj.startY); //设置起点状态
            this.ctx.lineTo(obj.endX, obj.endY); //设置末端状态
            this.ctx.strokeStyle = obj.color; //设置线的颜色状态
            this.ctx.stroke(); //进行绘制
        }

        for (let i = 0; i < this.circles.length; i++) {
            const obj = this.circles[i];
            if (obj.data == 0) continue;
            this.ctx.fillStyle = obj.color;
            this.ctx.beginPath();
            this.ctx.arc(obj.x, obj.y, obj.r, 0, 2 * Math.PI);
            // this.ctx.colsePath();

            this.ctx.fill();
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";

            const textX = obj.x - 7 * obj.data.toString().length; // 文字起点横坐标
            const textY = obj.y; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);

        }

        for (let i = 0; i < this.ansCircle.length; i++) {
            const obj = this.ansCircle[i];
            if (obj.data == 0) continue;
            this.ctx.fillStyle = obj.color;
            this.ctx.beginPath();
            this.ctx.arc(obj.x, obj.y, obj.r, 0, 2 * Math.PI);
            // this.ctx.colsePath();

            this.ctx.fill();
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";

            const textX = obj.x - 7 * obj.data.toString().length; // 文字起点横坐标
            const textY = obj.y; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);

        }
    }
}