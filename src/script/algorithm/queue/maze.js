import { GameObject } from './../../GameObject';
import { people } from './../../people';
import { wall } from '../../wall'
import { ElMessage } from 'element-plus'


export class maze extends GameObject {
    constructor(ctx, datas) {
        super();

        this.ctx = ctx;

        this.L = 30;

        this.g = [];
        this.rows = 15;
        this.cols = 15;

        this.startR = datas[0];
        this.startC = datas[1];
        this.endR = datas[2];
        this.endC = datas[3];
        this.inner_walls_count = 55;
        this.walls = [];
        this.dr = [-1, 0, 1, 0]; // 4个方向行的偏移量
        this.dc = [0, 1, 0, -1]; // 4个方向列的偏移量
        this.peoples = [];
        this.speed = 5;

        this.visted_color = [0, 0, "#454545", "#999999", "#f56c6c", "#A2D149"]
        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 10; // 代码监视的睡眠
        this.status = 'stop'; // stop:停止运动  running:正在运动  end:结束运动

        this.var_tx = 0;
        this.var_ty = 0;
        this.var_next_x = 0;
        this.var_next_y = 0;

    }

    computed_speed() {
        if (this.speed === 10) {
            return 10;
        } else if (this.speed === 20) {
            return 5;
        } else {
            return 20;
        }
    }

    create_walls() {
        const g = this.g
        for (let r = 0; r < this.rows; r++) {
            g[r] = [];
            for (let c = 0; c < this.cols; c++) {
                g[r][c] = 0;
            }
        }

        // 给四周加上障碍物
        for (let r = 0; r < this.rows; r++) {
            this.g[r][0] = this.g[r][this.cols - 1] = 1;
        }

        for (let c = 0; c < this.cols; c++) {
            this.g[0][c] = this.g[this.rows - 1][c] = 1;
        }
        this.visted = JSON.parse(JSON.stringify(this.g)); // 存放已经访问过的地方  1和0表示无权访问，2表示已经访问过的





        // 创建随机障碍物

        for (let i = 0; i < this.inner_walls_count; i++) {
            for (let j = 0; j < 1000; j++) {
                let r = parseInt(Math.random() * this.rows);
                let c = parseInt(Math.random() * this.cols);
                if (this.g[r][c] || (r === this.startR && c === this.startC) || (r === this.endR && c === this.endC)) continue;
                this.g[r][c] = 1;
                break;
            }
        }

        const copy_g = JSON.parse(JSON.stringify(this.g));
        // if (!this.check_connectivity(copy_g, this.st, 1, 1, this.cols - 2))
        //     return false;


        for (let r = 0; r < this.rows; r++) {
            for (let c = 0; c < this.cols; c++) {
                if (g[r][c]) {
                    this.walls.push(new wall(r, c, this, this.walls));
                }
            }
        }

        this.peoples.push(new people(this.startR, this.startC, 'man', this, this.peoples));
        this.peoples.push(new people(this.endR, this.endC, 'woman', this, this.peoples));


    }


    async dfs(tx, ty) {
        this.var_tx = tx;
        this.var_ty = ty;
        this.now_runing = 1;
        await this.sleep(this.computed_speed());

        this.visted[tx][ty] = 2;
        if (this.status === 'end') {
            return;
        }
        if (tx == this.endR && ty == this.endC) {
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            this.status = 'end';
            this.now_runing = 3;
            await this.sleep(this.computed_speed());
            this.peoples[0].status = 'end';
            this.now_runing = 4;
            await this.sleep(this.computed_speed());
            return true;
        }


        for (let i = 0; i < 4; i++) {
            this.now_runing = 5;
            await this.sleep(this.computed_speed());
            let next_x = tx + this.dr[i];
            this.now_runing = 6;
            await this.sleep(this.computed_speed());
            let next_y = ty + this.dc[i];
            this.now_runing = 7;
            await this.sleep(this.computed_speed());
            if (this.status === 'end') {
                return
            }
            this.now_runing = 8;
            await this.sleep(this.computed_speed());
            this.var_next_x = next_x;
            this.var_next_y = next_y;
            if ((this.g[next_x][next_y] === 0 || this.g[next_x][next_y] == 2) && this.visted[next_x][next_y] === 0) {
                if (this.status === 'end') {
                    return
                }


                await this.peoples[0].move_to(next_x, next_y);
                this.now_runing = 9;
                await this.sleep(this.computed_speed());
                this.visted[next_x][next_y] = 2;
                this.now_runing = 10;
                await this.sleep(this.computed_speed());
                await this.dfs(next_x, next_y);
                if (this.status === "end") {
                    return
                }
                await this.peoples[0].move_to(tx, ty);
                this.render();
                this.now_runing = 10;
                await this.sleep(this.computed_speed());
                this.visted[next_x][next_y] = 3;
            }

        }
        await this.sleep(this.computed_speed());
    }

    check_connectivity(g, sx, sy, tx, ty) {
        if (sx == tx && sy == ty) return true;
        g[sx][sy] = true;

        let dx = [-1, 0, 1, 0],
            dy = [0, 1, 0, -1];
        for (let i = 0; i < 4; i++) {
            let x = sx + dx[i],
                y = sy + dy[i];
            if (!g[x][y] && this.check_connectivity(g, x, y, tx, ty))
                return true;
        }

        return false;
    }
    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    start() {
        this.create_walls();
        // this.peoples[0].move_to(9, 9)
    }

    async work() {
        this.now_runing = 0;
        this.status = 'running';

        await this.dfs(this.startR, this.startC);
        this.status = 'end';
        if ((this.peoples[0].r === this.startR && this.peoples[0].c === this.startC) && (this.startC !== this.endC && this.startR !== this.endR)) {
            this.result = 'faild';
            ElMessage({
                showClose: true,
                message: "未能找到终点",
                type: 'warning',
                duration: 2000,
            })
        } else {
            this.result = 'suceess';
            ElMessage({
                showClose: true,
                message: "动画演示完成",
                type: 'success',
                duration: 2000,
            })
        }
        this.now_runing = -1;

    }

    update() {
        // this.update_size();
        this.render();
        this.update_status();
    }
    update_size() {
        this.L = parseInt(Math.min(this.parent.clientWidth / this.cols, this.parent.clientHeight / this.rows));
        this.ctx.canvas.width = this.L * this.cols;
        this.ctx.canvas.height = this.L * this.rows;
    }

    update_status() {
        for (let i = 0; i < this.peoples.length; i++) {
            const obj = this.peoples[i];
            obj.status = this.status;
            obj.speed = this.speed;
        }
    }

    flicker(outer) {
        if (outer.visted[outer.endR][outer.endC] === 4) {
            outer.visted[outer.endR][outer.endC] = 5;
        } else {
            outer.visted[outer.endR][outer.endC] = 4;
        }
        outer.ctx.fillRect(outer.endC * outer.L, outer.endR * outer.L, outer.L, outer.L);
    }


    on_destory() {
        while (this.walls.length > 0) {
            const obj = this.walls[0];
            obj.destory();
        }
        while (this.peoples.length > 0) {
            const obj = this.peoples[0];
            obj.destory();
        }
    }


    render() {

        if (this.status === 'end') {
            this.visted[this.peoples[0].r][this.peoples[0].c] = 4;
        }
        const color_even = "#A2D149",
            color_odd = "#AAD751";

        // 画地图底色
        for (let r = 0; r < this.rows; r++) {
            for (let c = 0; c < this.cols; c++) {
                if ((r + c) % 2 == 0) {
                    this.ctx.fillStyle = color_even;
                } else {
                    this.ctx.fillStyle = color_odd;
                }
                this.ctx.fillRect(c * this.L, r * this.L, this.L, this.L);
            }
        }

        // 画障碍物
        for (let i = 0; i < this.walls.length; i++) {
            const obj = this.walls[i];

            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.c * this.L, obj.r * this.L, this.L, this.L);

        }


        // 画出已经访问过的
        for (let r = 0; r < this.rows; r++) {
            for (let c = 0; c < this.cols; c++) {
                if (this.visted[r][c] == 0 || this.visted[r][c] == 1) continue;
                this.ctx.fillStyle = this.visted_color[this.visted[r][c]];
                this.ctx.fillRect(c * this.L, r * this.L, this.L, this.L);
            }
        }

        // 画人
        for (let i = 0; i < this.peoples.length; i++) {
            const obj = this.peoples[i];
            this.ctx.drawImage(obj.img, obj.c * this.L, obj.r * this.L, this.L, this.L);
        }
    }
}