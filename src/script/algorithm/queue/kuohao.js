import { GameObject } from './../../GameObject';
import { line } from './../../line';
import { rectangle } from './../../rectangle';
import { ElMessage } from 'element-plus'


export class kuohao extends GameObject {
    constructor(ctx, datas) {
        super();

        this.ctx = ctx;
        this.datas = datas;


        this.stack = []; // 当前栈内队列·
        this.lines = []; // 存放线
        this.wait = [];

        this.wait_begin_x = 350;
        this.wait_begin_y = 100;
        this.wait_width = 25;
        this.wait_height = 50;

        this.stack_begin_x = 150;
        this.stack_begin_y = 200;
        this.stack_width = 100;
        this.stack_height = 300;
        this.stack_now_height = this.stack_begin_y + this.stack_height - this.wait_height - 5;

        this.error_message = "";
        this.error_message_color = '#fff';

        this.speed = 200; // 每一帧移动速度
        this.status = "stop"; // 当前对象状态  stop:停止状态 running:运动状态 end:对象销毁
        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 100; // 代码监视的睡眠

        this.var_等待匹配长度 = 0;
        this.var_栈长度 = 0;
        this.var_now = '';

    }

    computed_speed() {
        if (this.speed === 200) {
            return 200;
        } else if (this.speed === 100) {
            return 400;
        } else {
            return 100;
        }
    }

    loadDatas(datas) {
        let nowX = this.wait_begin_x;
        let nowY = this.wait_begin_y;
        let color = '#3571FF';
        for (let i = 0; i < datas.length; i++) {
            this.wait.push(new rectangle(this.ctx, this.wait, i, nowX, nowY, this.wait_width, this.wait_height, datas[i], color));
            nowX += this.wait_width;
            this.wait[i].speed = 300;
        }
    }

    start() {
        this.lines.push(new line(this.ctx, this.stack_begin_x, this.stack_begin_y, this.stack_begin_x, this.stack_begin_y + this.stack_height, 'left', '#A37499', 5));
        this.lines.push(new line(this.ctx, this.stack_begin_x + this.stack_width, this.stack_begin_y, this.stack_begin_x + this.stack_width, this.stack_begin_y + this.stack_height, "right", '#A37499', 5));
        this.lines.push(new line(this.ctx, this.stack_begin_x, this.stack_begin_y + this.stack_height, this.stack_begin_x + this.stack_width, this.stack_begin_y + this.stack_height, "bottom", '#A37499', 7));
        this.loadDatas(this.datas);

        // this.work();
    }

    async work() {
        this.now_runing = 0;

        await this.run();
        if (this.stack.length === 0) {
            ElMessage({
                showClose: true,
                message: "括号匹配成功",
                type: 'success',
                duration: 2000,
            })
        } else {
            ElMessage({
                showClose: true,
                message: "括号匹配失败",
                type: 'error',
                duration: 2000,
            })
        }

        this.now_runing = -1;

    }

    async run() {
        let flag = {
            ')': '(',
            '}': '{',
            ']': '['
        }
        this.now_runing = 1;
        await this.sleep(this.computed_speed());
        this.var_等待匹配长度 = this.wait.length;
        while (this.wait.length > 0) {
            this.var_等待匹配长度 = this.wait.length;
            this.var_栈长度 = this.stack.length;
            this.now_runing = 2;
            await this.sleep(this.computed_speed());
            let now = this.wait[0];
            this.var_now = now.data;
            this.now_runing = 3;
            await this.sleep(this.computed_speed());
            if (now.data === '(' || now.data === '[' || now.data === '{') {
                this.error_message = "栈为空，左括号入栈";
                this.error_message_color = '#54AFEF'
                this.now_runing = 4;
                await this.sleep(this.computed_speed());
                await this.in_stack();
            } else {
                this.now_runing = 5;
                await this.sleep(this.computed_speed());
                if (this.stack.length === 0) {
                    this.now_runing = 6;
                    await this.sleep(this.computed_speed());
                    this.error_message_color = '#EB3941';
                    this.error_message = "栈为空，无法匹配";
                    return
                }

                this.now_runing = 7;
                await this.sleep(this.computed_speed());
                if (flag[now.data] === this.stack[this.stack.length - 1].data) {
                    this.now_runing = 8;
                    await this.sleep(this.computed_speed());
                    this.error_message = "右括号入栈,栈内有匹配左括号";
                    this.error_message_color = '#54AFEF'
                    this.now_runing = 9;
                    await this.sleep(this.computed_speed());
                    await this.out_stack();
                    this.wait[0].destory();
                    this.stack[this.stack.length - 1].destory();
                } else {
                    this.now_runing = 10;
                    this.error_message = "右括号入栈,栈内没有匹配左括号";
                    this.error_message_color = '#54AFEF'
                    await this.sleep(this.computed_speed());
                    return
                }
            }
        }






    }

    async in_stack() {

        this.wait[0].color = '#F1C40F'
        await this.wait[0].move_to(this.stack_begin_x + (this.stack_width - this.wait_width) / 2, this.wait_begin_y);
        await this.wait[0].move_to(this.stack_begin_x + (this.stack_width - this.wait_width) / 2, this.stack_now_height);
        this.wait[0].color = '#3571FF'

        this.stack_now_height -= this.wait_height;

        this.stack.push(this.wait[0])
        this.stack[this.stack.length - 1].parent = this.stack;
        this.wait.shift();
        this.wait_begin_x = this.wait[0].x - this.wait_width;
    }

    async out_stack() {
        this.stack[this.stack.length - 1].color = '#E74C3C'
        await this.stack[this.stack.length - 1].move_to(this.stack[this.stack.length - 1].x, this.wait_begin_y);
        await this.stack[this.stack.length - 1].move_to(this.wait_begin_x, this.stack[this.stack.length - 1].y);
        this.stack[this.stack.length - 1].color = '#3571FF'

        this.stack_now_height += this.wait_height;
    }

    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update() {
        this.render();
        this.update_spped();
        this.update_status();
    }

    update_status() {
        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            if (obj === null) continue;
            obj.status = this.status
        }
        for (let i = 0; i < this.wait.length; i++) {
            const obj = this.wait[i];
            if (obj === null) continue;

            obj.status = this.status
        }
        for (let i = 0; i < this.stack.length; i++) {
            const obj = this.stack[i];
            if (obj === null) continue;

            obj.status = this.status
        }
    }

    update_spped() {
        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
        for (let i = 0; i < this.wait.length; i++) {
            const obj = this.wait[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
        for (let i = 0; i < this.stack.length; i++) {
            const obj = this.stack[i];
            if (obj === null) continue;

            obj.speed = this.speed;
        }
    }



    on_destory() {
        while (this.lines.length > 0) {
            const obj = this.lines[0];
            obj.destory();
        }
        while (this.wait.length > 0) {
            const obj = this.wait[0];
            obj.destory();
        }
        while (this.stack.length > 0) {
            const obj = this.stack[0];
            obj.destory();
        }
    }


    render() {

        this.ctx.fillStyle = "#fff";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            this.ctx.lineWidth = obj.line_width; //设置线宽状态
            this.ctx.beginPath();
            this.ctx.moveTo(obj.startX, obj.startY); //设置起点状态
            this.ctx.lineTo(obj.endX, obj.endY); //设置末端状态
            this.ctx.strokeStyle = obj.color; //设置线的颜色状态
            this.ctx.stroke(); //进行绘制
        }

        for (let i = 0; i < this.wait.length; i++) {
            const obj = this.wait[i];
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            //画字
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }

        for (let i = 0; i < this.stack.length; i++) {
            const obj = this.stack[i];
            this.ctx.fillStyle = obj.color;
            this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);

            this.ctx.strokeStyle = "#707B7C"; // 设置边框颜色
            this.ctx.lineWidth = 2; // 设置边框宽度
            this.ctx.strokeRect(obj.x, obj.y, obj.width, obj.height); // 绘制边框矩形

            //画字
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";
            const textX = obj.x + (obj.width - textSize.width) / 2; // 文字起点横坐标
            const textY = obj.y + obj.height / 2; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);
        }

        this.ctx.fillStyle = this.error_message_color;
        this.ctx.textBaseline = 'middle';
        this.ctx.fillText(this.error_message, 100, 50);
    }
}