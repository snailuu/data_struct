import { circle } from '../../circle';
import { line } from '../../line';
import { GameObject } from '../../GameObject';
import { ElMessage } from 'element-plus'
import { resolve } from 'path-browserify';


export class spanningTree extends GameObject {
    constructor(ctx, datas, mode) {
        super();
        this.ctx = ctx;
        this.datas = datas;
        this.circleR = 20;
        this.error_message = "";
        this.circles = [];
        this.lines = [];

        this.mode = mode; // 判断是哪种生成树算法
        this.eps = 10; // 每一帧移动的距离
        this.has_called_start = true; //有没有执行过


        this.speed = 100; // 每一帧移动速度
        this.now_runing = 0; // 当前运行到的行号
        this.sleep_ms = 300; // 代码监视的睡眠
        this.start();

        this.var_确定节点数 = 0;
        this.var_当前边 = '0->0';
    }

    computed_speed() {
        if (this.speed === 100) {
            return 100;
        } else if (this.speed === 200) {
            return 50;
        } else {
            return 200;
        }
    }

    async loadDatas(datas) {
        let lens = datas[0].length;
        let F = 1;

        for (let Q = 0; Q < 1000; Q++) {
            for (let i = 0; i < lens; i++) {
                let flag = 0;
                while (!flag) {
                    let x = Math.random() * 1000;
                    let y = Math.random() * 1000;
                    if (x > 50 && x < 650 - this.circleR * 2 && y > 50 && y < 450 - this.circleR * 2) {
                        this.circles.push(new circle(this.ctx, this.circles, x, y, this.circleR, i, i, "#E74C3C"));
                        flag = 1; //随机函数创建圆
                    }
                }
            }
            let u = 1;
            for (let i = 0; i < lens; i++) {
                for (let j = i + 1; j < lens; j++) {
                    let Xcha = this.circles[i].x - this.circles[j].x;
                    let Ycha = this.circles[i].y - this.circles[j].y;
                    let ans = Math.sqrt(Xcha * Xcha + Ycha * Ycha);
                    if (ans < 10 * this.circleR) u = 0;
                }
            } //圆

            // for (let i = 0; i < lens; i++) {
            //     for (let j = i + 1; j < lens; j++) {
            //         if (datas[i][j] == 0) continue;
            //         this.lines.push(new line(this.ctx, this.circles[i].x, this.circles[i].y,
            //             this.circles[j].x, this.circles[j].y, `${i}-${j}`, "#61666a", 3, this.lines));
            //         this.lines[this.lines.length - 1].data = datas[i][j];
            //     }
            // } //线



            // for (let i = 0; i < this.lines.length; i++) {
            //     for (let j = i + 1; j < this.lines.length; j++) {
            //         let Vi = [],
            //             Vj = [];
            //         Vi.push(this.lines[i].startX - this.lines[i].endX);
            //         Vi.push(this.lines[i].startY - this.lines[i].endY);
            //         //i 的方向向量
            //         Vj.push(this.lines[j].startX - this.lines[j].endX);
            //         Vj.push(this.lines[j].startY - this.lines[j].endY);
            //         // j 的方向向量
            //         if (Vi[0] == Vj[0] && Vi[1] == Vj[1]) {
            //             u = 0;
            //             break;
            //         } //向量平行或重合，直接摧毁

            //         let tani = (this.lines[i].endY - this.lines[i].startY) / (this.lines[i].endX - this.lines[i].startX);
            //         let tanj = (this.lines[j].endY - this.lines[j].startY) / (this.lines[j].endX - this.lines[j].startX);
            //         let tanans = (tanj - tani) / (1 + tani * tanj);
            //         let ct = Math.atan(tanans) * 180.0 / 3.1415926;
            //         if (ct < 20) {
            //             u = 0;
            //             break;
            //         }
            //     }
            // }

            if (Q == 999) break;
            if (!u) {
                for (let i = 0; i < this.circles.length; i++) {
                    const obj = this.circles[i];
                    obj.destory();
                    i--;
                }

                // for (let i = 0; i < this.lines.length; i++) {
                //     const obj = this.lines[i];
                //     obj.destory();
                //     i--;
                // }
            }
            F -= u;
            if (F == 0) break;
        }
        for (let i = 0; i < lens; i++) {
            for (let j = i + 1; j < Math.min(lens, datas[i].length); j++) {
                if (datas[i][j] === 0 || datas[i][j] === null) continue;
                this.lines.push(new line(this.ctx, this.circles[i].x, this.circles[i].y,
                    this.circles[j].x, this.circles[j].y, `${i}-${j}`, "#61666a", 3, this.lines));
                this.lines[this.lines.length - 1].data = datas[i][j];
            }
        } //线

    }
    async start() {
        this.loadDatas(this.datas);

    }
    async work() {
        this.now_runing = 0;

        if (this.mode === 'A') {
            await this.prim();
        } else {
            await this.kruskal();
        }
        this.clearLines();
        this.status = 'end';
        this.now_runing = -1;

        ElMessage({
            showClose: true,
            message: "动画演示完成",
            type: 'success',
            duration: 2000,
        })
    }

    update() {
        this.update_status();
        if (this.status !== 'end') {
            this.render();

        }
    }
    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }

    update_status() {
        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            obj.status = this.status;
            obj.speed = this.speed;
        }
        for (let i = 0; i < this.circles.length; i++) {
            const obj = this.circles[i];
            obj.status = this.status;
            obj.speed = this.speed;
        }
    }
    async prim() {
        let stack = [];
        this.now_runing = 2;
        await this.sleep(this.computed_speed());
        stack.push(0); //放入节点0开始扩展
        this.circles[stack.length - 1].color = "#7DCEA0";

        let lens = this.datas[0].length;
        this.now_runing = 3;
        await this.sleep(this.computed_speed());
        this.var_确定节点数 = stack.length;
        while (stack.length < lens) {
            let minVal = 1e9,
                startIndex = 0,
                endIndex = 0;
            this.now_runing = 4;
            await this.sleep(this.computed_speed());
            for (let i = 0; i < stack.length; i++) {
                for (let j = 0; j < lens; j++) {
                    if (j == stack[i])
                        continue;
                    let flag = 1;

                    if (this.datas[stack[i]][j] == 0) flag = 0;

                    for (let k = 0; k < stack.length; k++) {
                        if (stack[k] == j) flag = 0;
                    } //判断该点是否已经扩展

                    if (flag) {
                        if (this.datas[stack[i]][j] < minVal) {
                            minVal = this.datas[stack[i]][j];
                            startIndex = stack[i]; //获取扩展的最小边值，以及节点
                            endIndex = j; //起点和终点
                            this.var_当前边 = `${startIndex}->${endIndex}`;
                        }
                    }

                }

            }
            this.now_runing = 5;
            await this.sleep(this.computed_speed());
            for (let i = 0; i < this.lines.length; i++) {
                if (this.lines[i].key === `${startIndex}-${endIndex}`) {
                    this.lines[i].color = "#409eff";
                }
            } //找到该直线

            for (let i = 0; i < this.lines.length; i++) {
                if (this.lines[i].key === `${endIndex}-${startIndex}`) {
                    this.lines[i].color = "#409eff";
                }
            } //找到该直线

            await this.sleep(this.computed_speed());
            stack.push(endIndex);
            this.circles[endIndex].color = "#7DCEA0";
            this.var_确定节点数 = stack.length;

            // this.render();
        } //扩展到所有点形成生成树
    }


    async kruskal() {
        this.sleep_ms = 100;
        let stack = [];
        let lens = this.datas[0].length;
        this.now_runing = 2;
        await this.sleep(this.computed_speed());
        this.lines.sort((a, b) => a.data - b.data);
        this.now_runing = 3;
        await this.sleep(this.computed_speed());
        for (let i = 0; i < lens - 1; i++) {
            this.var_确定节点数 = stack.length;
            let frist;
            let second;
            let nowLine;
            this.now_runing = 4;
            await this.sleep(this.computed_speed());
            for (let j = 0; j < this.lines.length; j++) {
                var m = this.lines[j].key.split("-");
                frist = m[0];
                second = m[1];
                this.now_runing = 5;
                await this.sleep(this.computed_speed());
                let flag = 0,
                    cnt = 0;
                this.now_runing = 6;
                await this.sleep(this.computed_speed());
                for (let k = 0; k < stack.length; k++) {
                    if (stack[k] == frist) cnt++;
                    if (stack[k] == second) cnt++; //判断该点是否在集合内
                }
                if (cnt != 2) {
                    flag = 1;
                    nowLine = j;
                    this.var_当前边 = this.lines[nowLine].key;
                } //判断两个点是否都选过


                this.now_runing = 7;
                await this.sleep(this.computed_speed());
                if (flag) {
                    break;
                }

            }

            this.lines[nowLine].color = "#409eff"; //选中线染色
            await this.sleep(300);
            let pointFlag = 0;
            for (let k = 0; k < stack.length; k++) {
                if (stack[k] == frist) pointFlag = 1;
            } //frist
            this.now_runing = 9;
            await this.sleep(this.computed_speed());
            if (!pointFlag) {
                stack.push(frist);
                this.circles[frist].color = "#7DCEA0";
                await this.sleep(300);
            }

            pointFlag = 0;

            for (let k = 0; k < stack.length; k++) {
                if (stack[k] == second) pointFlag = 1;
            } //second

            if (!pointFlag) {
                stack.push(second);
                this.circles[second].color = "#7DCEA0";
                await this.sleep(300);
            }

        }
    }


    clearLines() {
        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            if (obj.color === '#61666a') {
                obj.destory();
                i--;
            }
        }
        this.render();
        // return new Promise((resolve) => {


        //     resolve("finish");
        // })

    }

    sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        })
    }


    on_destory() {
        while (this.lines.length > 0) {
            const obj = this.lines[0];
            obj.destory();
        }
        while (this.circles.length > 0) {
            const obj = this.circles[0];
            obj.destory();
        }
    }

    async render() {

        this.ctx.fillStyle = "rgb(255, 255, 255)";
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.ctx.fillStyle = "#E74C3C";

        this.ctx.textBaseline = 'middle';
        this.ctx.fillText(this.error_message, 0, 700);

        // 把没用的都删了
        for (let i = 0; i < this.lines.length; i++) {
            const obj = this.lines[i];
            this.ctx.lineWidth = obj.line_width; //设置线宽状态
            this.ctx.beginPath();
            this.ctx.moveTo(obj.startX, obj.startY); //设置起点状态
            this.ctx.lineTo(obj.endX, obj.endY); //设置末端状态
            this.ctx.strokeStyle = obj.color; //设置线的颜色状态
            this.ctx.stroke(); //进行绘制还有一个问题
            //把边权画出来
            this.ctx.font = "bold 18px Arial";
            this.ctx.fillStyle = obj.color;
            this.ctx.textBaseline = 'middle';
            //算一下把，文字的xy  直线起点+直线终点/2
            this.ctx.fillText(obj.data, (obj.startX + obj.endX) / 2,
                (obj.startY + obj.endY) / 2);
        }
        //

        for (let i = 0; i < this.circles.length; i++) {
            const obj = this.circles[i];
            //if(obj.data == 0) continue;
            this.ctx.fillStyle = obj.color;
            this.ctx.beginPath();
            this.ctx.arc(obj.x, obj.y, obj.r, 0, 2 * Math.PI);
            // this.ctx.colsePath();

            this.ctx.fill();
            const textSize = this.ctx.measureText(obj.data);
            this.ctx.font = "bold 28px Arial";

            const textX = obj.x - 7 * obj.data.toString().length; // 文字起点横坐标
            const textY = obj.y; // 文字起点纵坐标
            this.ctx.fillStyle = '#fff';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillText(obj.data, textX, textY);

        }
    }
}