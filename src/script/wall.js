import { GameObject } from './GameObject';

export class wall extends GameObject {
    constructor(r, c, gamemap, parent) {
        super();

        this.r = r;
        this.c = c;
        this.gamemap = gamemap;
        this.color = '#B37226';
        this.parent = parent;
    }

    update() {
        if (this.status === 'running') {
            this.update_move();
        }
    }



    render() {
        const L = this.gamemap.L;
        const ctx = this.gamemap.ctx;

        ctx.fillStyle = this.color;
        ctx.fillRect(this.c * L, this.r * L, L, L);

    }
    on_destory() {
        for (let i = 0; i < this.parent.length; i++) {
            if (this.parent[i] === this) {
                this.parent.splice(i, 1);
                break;
            }
        }
    }
}