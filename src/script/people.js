import { GameObject } from './GameObject';

export class people extends GameObject {
    constructor(r, c, sex, gamemap, parent) {
        super();

        this.r = r;
        this.c = c;
        this.sex = sex;
        this.gamemap = gamemap;
        this.parent = parent;

        this.speed = 5; // 每秒走5个格子
        this.direction = -1; // -1表示没有操作，0、1、2、3表示上右下左
        this.move_length = 0;
        this.nextR = r;
        this.nextC = c;
        this.eps = 0.01;
        this.img = new Image();
        this.img.src = "https://api.snailuu.cn/data_struct/boy.png";
        if (this.sex === "woman") {
            this.img.src = "https://api.snailuu.cn/data_struct/girl.png";
        }
        this.status = 'stop';
    }

    update() {
        // if (this.status != 'end') {
        //     this.render();
        // }
        if (this.status === 'running') {
            this.update_move();
        }

    }

    get_dist(x1, y1, x2, y2) {
        let dx = x1 - x2;
        let dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    move_to(tx, ty) {
        // this.nextR = tx;
        // this.nextC = ty;
        this.move_length = this.get_dist(this.r, this.c, tx, ty);
        let angle = Math.atan2(ty - this.c, tx - this.r);
        this.vx = Math.cos(angle);
        this.vy = Math.sin(angle);
        return new Promise((resolve) => {

            const result = () => {
                if (this.move_length < this.eps) {
                    resolve("finish");
                    return;
                }
                requestAnimationFrame(result);
            }

            result();
        })
    }

    update_move() {

        if (this.move_length < this.eps) {
            this.move_length = 0;
            this.vx = this.vy = 0;
            this.r = Math.round(this.r)
            this.c = Math.round(this.c);
            this.status = "stop";
        } else {
            let moved = Math.min(this.move_length, this.speed * this.timedelta / 1000);
            this.r += this.vx * moved;
            this.c += this.vy * moved;
            this.move_length -= moved;
        }

        // const dx = this.r - this.nextR;
        // const dy = this.c - this.nextC;
        // const vx = dx > 0 ? 1 : -1;
        // const vy = dx > 0 ? 1 : -1;


        // this.move_length = Math.sqrt(dx * dx + dy * dy);
        // if (this.move_length < this.eps) {
        //     this.speed = 0;

        // } else {
        //     const moved = this.speed * this.timedelta / 1000;
        //     this.r += moved * dx * vx / this.move_length;
        //     this.c += moved * dy * vy / this.move_length;
        // }

    }

    render() {
        const L = this.gamemap.L;
        const ctx = this.gamemap.ctx;
        ctx.drawImage(this.img, this.c * L, this.r * L, L, L);
    }

    on_destory() {
        for (let i = 0; i < this.parent.length; i++) {
            if (this.parent[i] === this) {
                this.parent.splice(i, 1);
                break;
            }
        }
    }
}