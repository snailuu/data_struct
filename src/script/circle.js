import { GameObject } from './GameObject';
export class circle extends GameObject {
    constructor(ctx, parent, x, y, r, index, data, color) {
        super();
        this.ctx = ctx;
        this.parent = parent;
        this.x = x;
        this.y = y;
        this.r = r;
        this.index = index;
        this.data = data;
        this.color = color;

        this.move_length = 0; // 需要移动的距离长度
        this.speed = 100; // 移动速度
        this.eps = 0.01; // 误差
        this.status = "stop"; // 当前矩形块的状态  stop:停止 running:正在移动 end:排序完成
    }

    update() {
        // this.render();
        if (this.status === 'running') {
            this.update_move();
        }
    }

    get_dist(x1, y1, x2, y2) {
        let dx = x1 - x2;
        let dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    // 将当前对象移动到(tx, ty)
    move_to(tx, ty) {
        this.move_length = this.get_dist(this.x, this.y, tx, ty);
        let angle = Math.atan2(ty - this.y, tx - this.x);
        this.vx = Math.cos(angle);
        this.vy = Math.sin(angle);
        return new Promise((resolve) => {

            const result = () => {
                if (this.move_length < this.eps) {
                    resolve("finish");
                    return;
                }
                requestAnimationFrame(result);
            }

            result();
        })
    }

    update_move() {
        if (this.move_length < this.eps) {
            this.move_length = 0;
            this.vx = this.vy = 0;
            this.x = Math.ceil(this.x);
            this.status = "stop";
        } else {
            let moved = Math.min(this.move_length, this.speed * this.timedelta / 1000);
            this.x += this.vx * moved;
            this.y += this.vy * moved;
            this.move_length -= moved;
        }
    }

    on_destory() {
        for (let i = 0; i < this.parent.length; i++) {
            if (this.parent[i] === this) {
                this.parent.splice(i, 1);
                break;
            }
        }
    }

    render() {
        this.ctx.fillStyle = this.color;

        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
        // this.ctx.colsePath();

        this.ctx.fill();
        const textSize = this.ctx.measureText(this.data);
        this.ctx.font = "bold 28px Arial";
        const textX = this.x; // 文字起点横坐标
        const textY = this.y; // 文字起点纵坐标
        this.ctx.fillStyle = '#fff';
        this.ctx.textBaseline = 'middle';
        this.ctx.fillText(this.data, textX, textY);
    }
}