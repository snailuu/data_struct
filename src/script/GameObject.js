const GAME_OBJECTS = []

export class GameObject {
    constructor() {
        GAME_OBJECTS.push(this);
        this.timedelta = 0;
        this.has_called_start = false; //有没有执行过
    }


    // 对象实例化时执行一次(第一帧)
    start() {

    }

    // 每一帧执行一次，除了第一帧
    update() {

    }

    // 对象删除之前执行的操作
    on_destory() {

    }

    // 对象删除操作
    destory() {
        this.on_destory();

        for (let i in GAME_OBJECTS) {
            const obj = GAME_OBJECTS[i];
            if (obj === this) {
                // obj.destory();
                GAME_OBJECTS.splice(i, 1);
                break;
            }
        }
    }
}


let last_timestamp; // 上一次执行的时刻
const step = timestamp => {
    for (let obj of GAME_OBJECTS) {

        if (!obj.has_called_start) {
            obj.has_called_start = true;
            obj.start();
        } else {
            obj.timedelta = timestamp - last_timestamp;
            obj.update();
        }
    }

    last_timestamp = timestamp;

    // 递归调用，达到不断刷新效果
    requestAnimationFrame(step);
}

requestAnimationFrame(step);