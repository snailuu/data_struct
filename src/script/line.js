import { GameObject } from './GameObject';

export class line extends GameObject {
    constructor(ctx, startX, startY, endX, endY, key, color, line_width, parent) {
        super();
        this.ctx = ctx;
        this.startX = startX; // 开始点x坐标
        this.startY = startY; // 开始点y坐标
        this.endX = endX; // 结束点x坐标
        this.endY = endY; // 结束点y坐标
        this.key = key; // 当前直线的
        this.color = color; // 颜色
        this.line_width = line_width; // 直线的宽度
        this.parent = parent;
    }

    start() {

    }

    update() {
        // this.render();
    }

    render() {
        this.ctx.moveTo(this.startX, this.startY); //设置起点状态
        this.ctx.lineTo(this.endX, this.endY); //设置末端状态
        this.ctx.lineWidth = this.line_width; //设置线宽状态
        this.ctx.strokeStyle = this.color; //设置线的颜色状态
        this.ctx.stroke(); //进行绘制
    }

    on_destory() {
        for (let i = 0; i < this.parent.length; i++) {
            if (this.parent[i] === this) {
                this.parent.splice(i, 1);
                break;
            }
        }
    }

}