import { createRouter, createWebHashHistory } from 'vue-router'
import TestView from '../views/test/TestView.vue'


const router = createRouter({
    history: createWebHashHistory(
        import.meta.env.BASE_URL),
    routes: [
        { path: '/', redirect: '/WelcomeView' },
        { path: '/test', component: TestView },
        {
            path: '/WelcomeView',
            component: () =>
                import('../views/welcome/WelcomeView.vue')
        },
        {
            path: '/HomeView',
            component: () =>
                import('../views/home/HomeView.vue')
        },
        {
            path: '/Graphs',
            component: () =>
                import('../views/algorithms/find/Graphs.vue')
        },
        {
            path: '/Lists',
            component: () =>
                import('../views/algorithms/find/Lists.vue')
        },
        {
            path: '/Trees',
            component: () =>
                import('../views/algorithms/find/Trees.vue')
        },
        {
            path: '/Queue',
            component: () =>
                import('../views/algorithms/find/Queue.vue')
        },
        {
            path: '/Search',
            component: () =>
                import('../views/algorithms/find/Search.vue')
        },
        {
            path: '/FindHomeView',
            component: () =>
                import('../views/algorithms/find/FindHomeView.vue'),
            children: [{
                path: '/FindHomeView/DirectInsertionSort',
                component: () =>
                    import('../views/algorithms/sort/DirectInsertionSort.vue')
            },
            {
                path: '/FindHomeView/BubbleSortView',
                component: () =>
                    import('../views/algorithms/sort/BubbleSortView.vue')
            },
            {
                path: '/FindHomeView/MergeSort',
                component: () =>
                    import('../views/algorithms/sort/MergeSort.vue')
            },

            {
                path: '/FindHomeView/SimpleSelectionSort',
                component: () =>
                    import('../views/algorithms/sort/SimpleSelectionSort.vue')
            },

            ]
        },
        {
            path: '/coderunning',
            component: () =>
                import('../views/coderunning/CodeRuningView.vue')
        },
        {
            path: '/404',
            component: () =>
                import('../views/404/404NotFoundView.vue')
        },
        {
            path: "/:catchAll(.*)",
            redirect: "/404/"
        }
    ]
})

export default router