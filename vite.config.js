import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { fileURLToPath, URL } from 'node:url'
import { VitePWA } from 'vite-plugin-pwa'
// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        VitePWA({
            manifest: {
                name: '龙江猪脚饭',
                short_name: '猪脚饭',
                description: '数据结构算法可视化系统',
                theme_color: '#ffffff',
                icons: [ //添加图标， 注意路径和图像像素正确
                    {
                        src: 'https://api.snailuu.cn/data_struct/pwa/headphoto192-192.png',
                        sizes: '192x192',
                        type: 'image/png',
                    },
                    {
                        src: 'https://api.snailuu.cn/data_struct/pwa/headphoto192-192.png',
                        sizes: '512x512',
                        type: 'image/png',
                    },
                ]
            },
            registerType: 'autoUpdate',
            workbox: {
                globPatterns: ['**/*.{js,css,html,ico,png,svg}'], //缓存相关静态资源
                runtimeCaching: [
                    // 匹配 api.snailuu.cn 下的请求
                    {
                        handler: 'CacheFirst',
                        urlPattern: /^https:\/\/api\.snailuu\.cn/,
                        options: {
                            cacheName: 'snailuu-api-cache',
                            expiration: {
                                maxEntries: 10,
                                maxAgeSeconds: 60 * 60 * 24 * 30 // 30 days
                            },
                            cacheableResponse: {
                                statuses: [0, 200]
                            }
                        }
                    }, { //由于要测试第三方API， 这里配置缓存访问第三方API的资源
                        handler: 'CacheFirst',
                        urlPattern: /^https:\/\/jsonplaceholder/, //注意，要修改成要测试的API。请使用正则表达式匹配
                        method: 'GET',
                        options: {
                            cacheName: 'test-external-api', //创建缓存名称
                            expiration: {
                                maxEntries: 10,
                                maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
                            },
                            cacheableResponse: {
                                statuses: [0, 200]
                            }
                        }
                    }
                ]
            },
            devOptions: {
                enabled: true
            }
        })
    ],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src',
                import.meta.url))
        }
    }
})