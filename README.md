
<p align="center">
    <a href="https://pig.snailuu.cn" target="_blank" rel="noopener noreferrer">
        <img width="800" src="https://api.snailuu.cn/data_struct/pwa/logo1.png" alt="pig logo" />
    </a>
</p>


<p align="center"><b>龙江猪脚饭</b> 基于Vue+Element Plus的在线数据结构算法可视化演示系统，值得一试。</p>

<br />
<p align="center">
	<a href="https://gitee.com/snailuu/data_struct/contributors?ref=master">小组成员</a>
	<a href="https://gitee.com/snailuu/data_struct">Gitee仓库</a>
</p>

---


## 🌸 快速开始


- 1、本地环境：`node.js、npm`
- 2、编译器：`vscode` 或 `webstorm`
- 2、拉取或下载项目<https://gitee.com/snailuu/data_struct>；
- 3、命令行执行 `npm i` 安装依赖； 
- 4、命令行执行 `npm run dev` 启动项目；


## ⭐️ 功能清单

### 界面部分

1. - [x] 欢迎页
2. - [x] 算法引导页
3. - [x] 算法介绍页
4. - [x] 代码执行页

### 算法部分
1. - [x] 直接插入排序
2. - [x] 冒泡排序
3. - [ ] 快速排序
4. - [x] 简单选择排序
5. - [x] 归并排序
6. - [x] 顺序表的第 `i` 位插入
7. - [x] 顺序表的第 `i` 位删除
8. - [ ] 两个有序表的合并
9. - [x] 约瑟夫环
10. - [x] 括号匹配
11. - [x] 迷宫搜索问题
12. - [x] 二叉树的先序遍历(非递归)
13. - [x] 二叉树的中序遍历(非递归)
14. - [x] 二叉树的后序遍历(非递归)
15. - [x] 二叉树的层次遍历
16. - [ ] 图的深度优先遍历
17. - [ ] 图的广度优先遍历
18. - [x] prim算法求最小生成树
19.  - [x] krusal算法求最小生成树
20.  - [ ] 二叉排序树创建
21.  - [ ] 二叉排序树删除
22.  - [x] 哈希表线性探测法解决冲突
23.  - [x] 哈希表拉链法解决冲突

## 📃 TODO
1. - [ ] 暗黑模式
2. - [ ] 对不同尺寸的屏幕的动画自适应


## 🌈 在线体验

在浏览器地址栏输入：`pig.snailuu.cn`



## 🔍 部分截图

### 引导页

![引导页](https://api.snailuu.cn/data_struct/readme/%E6%AC%A2%E8%BF%8E%E9%A1%B5.png)

### 算法引导页

![引导页](https://api.snailuu.cn/data_struct/readme/%E7%AE%97%E6%B3%95%E5%BC%95%E5%AF%BC%E9%A1%B5.png)

### 算法介绍页

![引导页](https://api.snailuu.cn/data_struct/readme/%E7%AE%97%E6%B3%95%E4%BB%8B%E7%BB%8D%E9%A1%B5.png)

### 代码执行页

![引导页](https://api.snailuu.cn/data_struct/readme/%E4%BB%A3%E7%A0%81%E6%89%A7%E8%A1%8C%E9%A1%B5.png)

## 🏭 项目结构
```
data_struct
├─ .git
│  ├─ config
│  ├─ description
│  ├─ HEAD
│  ├─ hooks
│  │  ├─ applypatch-msg.sample
│  │  ├─ commit-msg.sample
│  │  ├─ fsmonitor-watchman.sample
│  │  ├─ post-update.sample
│  │  ├─ pre-applypatch.sample
│  │  ├─ pre-commit.sample
│  │  ├─ pre-merge-commit.sample
│  │  ├─ pre-push.sample
│  │  ├─ pre-rebase.sample
│  │  ├─ pre-receive.sample
│  │  ├─ prepare-commit-msg.sample
│  │  ├─ push-to-checkout.sample
│  │  └─ update.sample
│  ├─ index
│  ├─ info
│  │  └─ exclude
│  ├─ objects
│  │  ├─ info
│  │  └─ pack
│  │     ├─ pack-0ebc40ac7646d6373467668f570664a0501197cd.idx
│  │     └─ pack-0ebc40ac7646d6373467668f570664a0501197cd.pack
│  ├─ packed-refs
│  └─ refs
│     ├─ heads
│     │  └─ master
│     ├─ remotes
│     │  └─ origin
│     │     └─ HEAD
│     └─ tags
├─ .gitignore
├─ index.html
├─ package-lock.json
├─ package.json
├─ public
│  └─ vite.svg
├─ README.md
├─ src
│  ├─ App.vue
│  ├─ assets
│  │  ├─ font
│  │  │  └─ font.css
│  │  ├─ ico
│  │  │  └─ aliyun.css
│  │  ├─ images
│  │  │  └─ gif
│  │  │     ├─ cengci.gif
│  │  │     ├─ charu.gif
│  │  │     ├─ guibin.gif
│  │  │     ├─ haxilalian.gif
│  │  │     ├─ haxixunzhi.gif
│  │  │     ├─ houxu.gif
│  │  │     ├─ jiandan.gif
│  │  │     ├─ krusal.gif
│  │  │     ├─ kuohao.gif
│  │  │     ├─ maopao.gif
│  │  │     ├─ migong.gif
│  │  │     ├─ prim.gif
│  │  │     ├─ shanchu.gif
│  │  │     ├─ shunxubiao.gif
│  │  │     ├─ xianxu.gif
│  │  │     ├─ yuesefu.gif
│  │  │     └─ zhongxu.gif
│  │  ├─ img
│  │  │  └─ ghost-img.png
│  │  └─ vue.svg
│  ├─ components
│  │  └─ startCodeRunning.vue
│  ├─ main.js
│  ├─ router
│  │  └─ index.js
│  ├─ script
│  │  ├─ algorithm
│  │  │  ├─ find
│  │  │  │  └─ hashList.js
│  │  │  ├─ graph
│  │  │  │  └─ spanningTree.js
│  │  │  ├─ list
│  │  │  │  ├─ josephring.js
│  │  │  │  └─ shunxulist.js
│  │  │  ├─ queue
│  │  │  │  ├─ kuohao.js
│  │  │  │  └─ maze.js
│  │  │  ├─ sort
│  │  │  │  ├─ bubble.js
│  │  │  │  ├─ choice.js
│  │  │  │  ├─ guibing.js
│  │  │  │  └─ insert.js
│  │  │  └─ tree
│  │  │     └─ tree.js
│  │  ├─ circle.js
│  │  ├─ GameObject.js
│  │  ├─ line.js
│  │  ├─ people.js
│  │  ├─ rectangle.js
│  │  └─ wall.js
│  ├─ stores
│  │  ├─ codes.js
│  │  ├─ codeVar.js
│  │  ├─ datas.js
│  │  ├─ index.js
│  │  └─ message.js
│  ├─ style.css
│  └─ views
│     ├─ 404
│     │  └─ 404NotFoundView.vue
│     ├─ algorithms
│     │  ├─ find
│     │  │  ├─ FindHomeView.vue
│     │  │  ├─ Graphs.vue
│     │  │  ├─ Lists.vue
│     │  │  ├─ Queue.vue
│     │  │  ├─ Search.vue
│     │  │  └─ Trees.vue
│     │  ├─ graph
│     │  │  ├─ Depth.vue
│     │  │  └─ MinTree.vue
│     │  ├─ list
│     │  │  ├─ Delect.vue
│     │  │  ├─ Insert.vue
│     │  │  └─ JosephRing.vue
│     │  ├─ queue
│     │  │  ├─ maze.vue
│     │  │  └─ parenthesis.vue
│     │  ├─ search
│     │  │  ├─ liness.vue
│     │  │  └─ zipper.vue
│     │  ├─ sort
│     │  │  ├─ BubbleSortView.vue
│     │  │  ├─ DirectInsertionSort.vue
│     │  │  ├─ MergeSort.vue
│     │  │  └─ SimpleSelectionSort.vue
│     │  └─ tree
│     │     ├─ Epilogue.vue
│     │     ├─ InfixOrder.vue
│     │     ├─ LeOrder.vue
│     │     └─ Priority.vue
│     ├─ coderunning
│     │  └─ CodeRuningView.vue
│     ├─ home
│     │  └─ HomeView.vue
│     ├─ test
│     │  └─ TestView.vue
│     └─ welcome
│        └─ WelcomeView.vue
└─ vite.config.js

```